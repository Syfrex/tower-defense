﻿using UnityEngine;
using System.Collections;

public class ShieldHealth : MonoBehaviour {

    private float currentshield;
    private float scalesize;
    private float startscale;

	void Start () 
    {
        currentshield = transform.parent.GetComponent<DrillbossHealth>().ShieldHealth;
        scalesize = transform.parent.GetComponent<DrillbossHealth>().ShieldHealth / currentshield;
        startscale = this.transform.localScale.x;
	
	}
	
	// Update is called once per frame
	void Update () {
        scalesize = transform.parent.GetComponent<DrillbossHealth>().ShieldHealth / currentshield;

        this.gameObject.transform.localScale = new Vector2(startscale * scalesize, this.transform.localScale.y);
	}
}
