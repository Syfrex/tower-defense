﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(DrawSplinePath))]
public class SplineLineInspector : Editor
{
    private void OnSceneGUI()
    {
        DrawSplinePath splineLine = target as DrawSplinePath;
        Transform handleTransform = splineLine.transform;
        Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;
        Vector3 p0 = handleTransform.TransformPoint(splineLine.p0);
        Vector3 p1 = handleTransform.TransformPoint(splineLine.p1);

        Handles.color = Color.white;
        Handles.DrawLine(p0, p1);
            //line start
        EditorGUI.BeginChangeCheck();
        p0 = Handles.DoPositionHandle(p0, handleRotation);
        if(EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(splineLine, "Move Point");
            EditorUtility.SetDirty(splineLine);
            splineLine.p0 = handleTransform.InverseTransformPoint(p0);
        }
            //line stop
        EditorGUI.BeginChangeCheck();
        p1 = Handles.DoPositionHandle(p1, handleRotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(splineLine, "Move Point");
            EditorUtility.SetDirty(splineLine);
            splineLine.p1 = handleTransform.InverseTransformPoint(p1);
        }
    }

}
