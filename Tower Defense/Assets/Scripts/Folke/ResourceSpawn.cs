﻿using UnityEngine;
using System.Collections;

public class ResourceSpawn : MonoBehaviour
{

    public float SpawnRate = 1.0f;
    //public float Spawnrate = 1.0f;
    //public float WhenToActive = 0f;
    public float SpeedWhenSpawn = 1.0f;
    public static float Speed = 0.1f;
    public static float spawn;
    public GameObject ResourcePrefab;
    private GameObject ResourceSpawned;
    public float StartSpawn = 5.0f;
    //private GameObject currencyHUD;


    // Use this for initialization
    void Start()
    {
        Speed = SpeedWhenSpawn;
        spawn = SpawnRate;
        SpawnRate = StartSpawn;
        //currencyHUD = GameObject.Find("Canvas").transform.GetChild(1).gameObject;
        //SpawnRate = Spawnrate;
        //InvokeRepeating("GoldSpawnPoint", WhenToActive, SpawnRate);
    }

    void SpawnPoint()
    {
        ResourceSpawned = (GameObject)Instantiate(ResourcePrefab, transform.position, transform.rotation);
        //ResourceSpawned.transform.parent = currencyHUD.transform;
        //GoldSpawned.transform.parent = this.transform;
    }

    void Update()
    {

        if(SpawnRate <= 0)
        {
            SpawnPoint();
            SpawnRate = spawn;
        }
        else
        {
            SpawnRate -= Time.deltaTime;
        }

    }
}
