﻿using UnityEngine;
using System.Collections;

public class WaveHandler : MonoBehaviour {

    public static float cooldown = 0;
    public float FirstWaveIncoming = 30;
    public static int EnemiesAlive = 0;  //Amount of enemis alive so that the wave doesn't update before everyone is dead
    public static float WaveTimer = 0; //Countdown to the next wave
    public float CooldownTimer = 30;
    private GameObject[] enemyspawnpoints;
    private GameObject[] Bossspawnpoints;

    // Waves to Pass
    public int howManyLevelsToPass;
    StopTime stopTime = new StopTime();
    public GameObject victoryScreen;

    void Start () 
    {
        AudioListener.volume = 1;
        MineScript ms = new MineScript();
        //cooldown = CooldownTimer;
        enemyspawnpoints = GameObject.FindGameObjectsWithTag("EnemySpawn");
        Bossspawnpoints = GameObject.FindGameObjectsWithTag("BossSpawn");
        cooldown = FirstWaveIncoming;
        InvokeRepeating("Wavetimer", 0, 1.0f); //Enough with updating the timer once every second
        EnemiesAlive = 0;
	}
    void Wavetimer()
    {
        WaveTimer += 1;
    }
	
	void Update () 
    {
            if (cooldown > 0 && EnemiesAlive <= 0)
            {
                cooldown -= Time.deltaTime;
                for (int i = 0; i < enemyspawnpoints.Length ; i++)
                {
                    if (enemyspawnpoints[i].GetComponent<EnemySpawn>().WhenOnWaveToActive < enemyspawnpoints[i].GetComponent<EnemySpawn>().wavetimetoactive)
                    {
                        enemyspawnpoints[i].GetComponent<EnemySpawn>().WhenOnWaveToActive = enemyspawnpoints[i].GetComponent<EnemySpawn>().wavetimetoactive;

                    }
                }
                for (int i = 0; i < Bossspawnpoints.Length; i++)
                {
                    if (Bossspawnpoints[i].GetComponent<BossSpawn>().WhenOnWaveToActive < Bossspawnpoints[i].GetComponent<BossSpawn>().wavetimetoactive)
                    {
                        Bossspawnpoints[i].GetComponent<BossSpawn>().WhenOnWaveToActive = Bossspawnpoints[i].GetComponent<BossSpawn>().wavetimetoactive;

                    }

                }
            }
            else if (cooldown <= 0)
            {

                Wavecount();

            cooldown = CooldownTimer;
            }
	}

    void Wavecount()
    {
        if (HUDCounter.WaveCounter == howManyLevelsToPass)
        {
            TrainScript.alterTrainSpeed = false;
            TrainScript.currentTrainSpeed = 0f;
            FUCKMESCRIPT.fulFuckINGHELL = 0; //Friend of mine that was a bit tierd of his programming 
            stopTime.PauseTime();
            victoryScreen.SetActive(true);
        }

        WaveTimer = 0;

        HUDCounter.WaveCounter += 1;
    }
}
