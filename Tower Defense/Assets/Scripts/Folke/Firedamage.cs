﻿using UnityEngine;
using System.Collections;

public class Firedamage : MonoBehaviour 
{


    void OnTriggerStay2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Enemy" && coll.gameObject.name == "Bomber")
        {
            coll.GetComponent<Enemy>().CurrentHealth -= this.transform.parent.GetComponent<FlameThrower>().Damage;

        }
        else if (coll.gameObject.tag == "Enemy" && coll.gameObject.name == "Boss")
        {
            coll.GetComponent<BossScript>().CurrentHealth -= this.transform.parent.GetComponent<FlameThrower>().Damage;

        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {

    }
}
