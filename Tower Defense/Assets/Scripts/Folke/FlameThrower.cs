﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class FlameThrower : MonoBehaviour {

    [Header("Bullet Propeties: ")]

   public AudioClip BulletSound;
   private AudioSource bulletsound;
   private Transform target;

   public GameObject bullet;
   public GameObject flames;
   public Transform bulletSpawn;
   public float BulletSpeed = 50;
   //public Transform startsize;
   private int PlayCurrentFrame = 0;

   [Header("Turret Propeties: ")]
   // public GameObject TurretVision;

 //   [HideInInspector]
    public float Range;// = 3.5f;
  //  [HideInInspector]
    public float Damage = 3.0f;
   // [HideInInspector]
    public float turnSpeed = 10f;
   // [HideInInspector]
    public float FirePerSecond = 1f;

    public int SoundPerFrame = 1;
    private float fireCountdown = 0f;
    private float fireeffectCD = 0f;


    private float distancetoEnemy;
	void Start () 
    {
       // startsize = transform;
        bulletsound = GetComponent<AudioSource>();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        InvokeRepeating("CheckSize", 0f, 1f);

       // animation.speed = (1 / BulletsPerSecond);
	}
    void CheckSize() //failsafe in case the size turns out bigger than it should 
    {
        if (this.transform.localScale.x < 0.1)
        {
            this.transform.localScale = new Vector3(0.33f, 0.33f, 0.33f);
        }
        else
        {
            CancelInvoke("CheckSize");
        }
    }
    void UpdateTarget()
    {
    
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float ShortestDistance = Mathf.Infinity; //To avoid null or errors by telling that this variable will be a positive number
        GameObject NearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            distancetoEnemy = Vector3.Distance(new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), new Vector3(enemy.transform.position.x, enemy.transform.position.y,0));    
            if(distancetoEnemy < ShortestDistance)
            {
                ShortestDistance = distancetoEnemy;
                NearestEnemy = enemy;
                if(target == null)
                {
                    target = enemy.transform;
                }

            }
        }


        if(NearestEnemy != null && ShortestDistance <= Range)
        {
            target = NearestEnemy.transform;
        }
        else
        {
            target = null;
        }

    }

    void Update()
    {
        
        if (target == null)
        {
            if(transform.GetChild(0).gameObject == isActiveAndEnabled)
            {
                transform.GetChild(0).gameObject.SetActive(false);
            }
            return;
        }


        Vector3 dir = target.position - (transform.position);
         float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        this.transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * turnSpeed);
        transform.GetChild(0).gameObject.SetActive(true);

        PlayCurrentFrame--; 
        if (fireCountdown <= 0)
        {
            //if(typeOfTurret == Draggable.TurretType.FASTDMG)
            //{ 
            if (PlayCurrentFrame <= 0)
            {

                PlayCurrentFrame = SoundPerFrame;
            }

            GameObject flameIns = (GameObject)Instantiate(flames, new Vector3(bulletSpawn.position.x, bulletSpawn.position.y, -3), this.transform.rotation);

               // flameIns.transform.parent = this.transform;
                flameIns.transform.Rotate(0, 0, 90);
            //flames.transform.position = bulletSpawn.position;
                //flames.transform.position = this.transform.position;
                //flames.transform.rotation
                
            
           // ShootDMG();
            fireCountdown = 1f / FirePerSecond;
        }
        else
        {
            fireCountdown -= Time.deltaTime;
        }
    }

    void ShootDMG()
    {
        GameObject bulletGameobject = (GameObject)Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        bulletGameobject.transform.parent = this.transform;
        FlameBullet bulletGo = bulletGameobject.GetComponent<FlameBullet>();

        if (bulletGo != null)
        {
            bulletGo.Seek(target);
        }
    }
    
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(this.transform.position, Range);
    }
}
