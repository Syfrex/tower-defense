﻿using UnityEngine;
using System.Collections;

public class TurretVision : MonoBehaviour 
{
    private Transform TurretParent;
	// Use this for initialization
	void Start () 
    {

        TurretParent = this.transform.parent.transform;
        transform.localScale = new Vector3(TurretParent.transform.GetComponent<Turret>().Range , TurretParent.transform.GetComponent<Turret>().Range, 0) * (1 / TurretParent.localScale.x);
	}

}
