﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndScript : MonoBehaviour 
{
    public int killCounter;// = HUDText.KillCounter;
    public int waveCounter;// = HUDText.WaveCounter;
    public int baseHealth;// = HUDText.WaveCounter;
    //[Header("Game Over:")]
    //public string GOText = "GAME OVER!!!";
    //public GUIStyle Gameoverpropeties;
    //public int GOX = 10;
    //public int GOY = 10;
    //public int GOW = 150;
    //public int GOH = 20;
    //[Header("Wave Counter config:")]
    //public string WaveText = "WAVE: ";
    //public GUIStyle WaveTextpropeties;
    //public int WavePosX = 10;
    //public int WavePosY = 10;
    //public int WaveSizeW = 150;
    //public int WaveSizeH = 20;
    //[Header("Kill Counter config:")]
    //public string KillText = "KILL COUNTER: ";
    //public GUIStyle KillTextpropeties;
    //public int KillPosX = 10;
    //public int KillPosY = 10;
    //public int KillSizeW = 150;
    //public int KillSizeH = 20;
    //[Header("Countdown Counter config:")]
    //public string CountdownText = "Going to Menu in: ";
    //public GUIStyle CountdownTextpropeties;
    //public int CountdownPosX = 10;
    //public int CountdownPosY = 10;
    //public int CountdownSizeW = 150;
    //public int CountdownSizeH = 20;
    //[Header("ScenePropeties:")]
    //public string ChangeSceneTo;
    //public float endscreentime = 30;
    //private float endscrn;

    //void Start()
    //{
    //    KillCounter = HUDText.KillCounter;
    //    WaveCounter = HUDCounter.WaveCounter;
    //    //endscrn = endscreentime;
    //}

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(Screen.width /2  - GOX,  GOY, GOW, GOH),GOText,  Gameoverpropeties);
    //    GUI.Label(new Rect(Screen.width / 2 - WavePosX, Screen.height / 2 - WavePosY, WaveSizeW, WaveSizeH), WaveText + WaveCounter, WaveTextpropeties);
    //    GUI.Label(new Rect(Screen.width / 2 - KillPosX, Screen.height / 2 - KillPosY, KillSizeW, KillSizeH), KillText + KillCounter, KillTextpropeties);
    //    GUI.Label(new Rect(Screen.width / 2 - CountdownPosX, Screen.height / 2 - CountdownPosY, CountdownSizeW, CountdownSizeH), CountdownText + (int)endscreentime, CountdownTextpropeties);
    //}

    //void Update()
    //{
    //    if(endscreentime <= 0)
    //    {
    //        //endscreentime = endscrn;

    //        SceneManager.LoadScene(ChangeSceneTo, LoadSceneMode.Single);
    //    }
    //    else
    //    {
    //        endscreentime -= Time.deltaTime;
    //    }
    //}

    //public void DisplayEndScore()
    void Start()
    {
        killCounter = HUDText.KillCounter;
        waveCounter = HUDCounter.WaveCounter;
        baseHealth = (int)MineScript.Health;
        this.GetComponent<Text>().text = "Killcount: " + killCounter + "\n"
            + "Wavecounter: " + waveCounter + "\n"
            + "Basehealth: " + baseHealth;
    }
}