﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{

    public float speed = 10.0f; // speed of camera
    private float margin = 1.0f; // amount of movement
    private float zoom = 1.0f; // zoom speed
    public float zoom_out = 20; // minimum zoom for camera
    public float zoom_in = 3; // maximum zoom for the camera
    
    public float max_xBounds = 100; // the bounds from center xBounds and -xBounds will be the limit
    public float min_xBounds = -100; // the bounds from center xBounds and -xBounds will be the limit
    public float max_yBounds = 100; // the bounds from center zBounds and -zBounds will be the limit
    public float min_yBounds = -100; // the bounds from center zBounds and -zBounds will be the limit

    private GameObject wavecounter;
    private GameObject wavecounterafterten;

    void Update()
    {
        if (gameObject.transform.GetChild(0) != null)
        {
            if (WaveHandler.cooldown < 5)
            {
                gameObject.transform.GetChild(0).gameObject.SetActive(true);
                gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject.SetActive(true);
                if (HUDCounter.WaveCounter >= 9)
                {
                    gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject.SetActive(true);
                    wavecounterafterten = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter + 1).gameObject;
                }
                //if (HUDCounter.WaveCounter == 9)
                //{
                //    gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject.SetActive(true);
                //    wavecounterafterten = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject;

                //}
                wavecounter = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject;
            }
            else
            {
                gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject.SetActive(false);
                if (wavecounter == isActiveAndEnabled)
                {
                    wavecounter.SetActive(false);
                }
                if (wavecounterafterten == isActiveAndEnabled)
                {
                    wavecounterafterten.SetActive(false);
                }
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }


        // Init camera translation for this frame.
        var move = Vector3.zero; // set it to (0,0,0)

        //Camera movement with keys and mouse
       if (Input.GetAxis("Vertical") > 0 || Input.mousePosition.y > Screen.height * 0.995f) // forward  
            {
                move = Vector3.up;//new Vector3(0, margin, 0); // because of the 40 degree tilt on camera we need to move in x and z at the same time
                
//            move = new Vector3(margin, 0, margin); // because of the 40 degree tilt on camera we need to move in x and z at the same time
            }
            if (Input.GetAxis("Vertical") < 0 || Input.mousePosition.y < Screen.height * 0.009f) // backward 
            {
                move = Vector3.down;//new Vector3(0, -margin, 0); // because of the 40 degree tilt on camera we need to move in x and z at the same time
//                move = new Vector3(-margin, 0, -margin); // because of the 40 degree tilt on camera we need to move in x and z at the same time
        }
            if (Input.GetAxis("Horizontal") > 0 || Input.mousePosition.x > Screen.width * 0.995f) // right 
            {
                move = new Vector3(margin, 0, 0); // because of the 40 degree tilt on camera we need to move in x and z at the same time
//                move = new Vector3(margin, 0, -margin); // because of the 40 degree tilt on camera we need to move in x and z at the same time
        }
            if (Input.GetAxis("Horizontal") < 0 || Input.mousePosition.x < Screen.width * 0.009f) // left 
            {
                move = new Vector3(-margin, 0, 0); // because of the 40 degree tilt on camera we need to move in x and z at the same time
//                move = new Vector3(-margin, 0, margin); // because of the 40 degree tilt on camera we need to move in x and z at the same time
            }
        //Camera zoom
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && Camera.main.orthographicSize < zoom_out)
        {
            Camera.main.orthographicSize += zoom;

        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && Camera.main.orthographicSize > zoom_in)
        {
            Camera.main.orthographicSize -= zoom;
        }
        if (this.transform.position.x < (-min_xBounds - 1))
            this.transform.position.Set(-min_xBounds, 0, 0);
        if (this.transform.position.x < (max_xBounds + 1))
            this.transform.position.Set(max_xBounds, 0, 0);
        if (this.transform.position.y < (-min_yBounds - 1))
            this.transform.position.Set(0, 0, -min_yBounds);
        if (this.transform.position.y < (max_yBounds + 1))
            this.transform.position.Set(0, 0, max_yBounds);


        var LevelCenter = transform.position + move;
        if (LevelCenter.x < min_xBounds || max_xBounds < LevelCenter.x)
        {
            move.x = 0;
        }
        if (LevelCenter.z < zoom_out || zoom_in < LevelCenter.z)
        {
            move.z = 0;
        }
        if (LevelCenter.y < min_yBounds || max_yBounds < LevelCenter.y)
        {
            move.y = 0;
        }
        // last thing we do is move the camera
        transform.position += move * speed * Time.deltaTime;
        // Debug.Log(move);
        // Keep camera within level and zoom area


        //reset the camera to bounds if it extends them
   
    }
}
