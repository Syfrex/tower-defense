﻿using UnityEngine;
using System.Collections;

public class EnemyBlackHealthBar : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {

        this.gameObject.transform.localScale = new Vector2(transform.parent.GetComponent<Enemy>().CurrentHealth / 10, this.transform.localScale.y);
	}
	
	
}
