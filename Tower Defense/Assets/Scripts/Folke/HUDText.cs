﻿using UnityEngine;
using System.Collections;

public class HUDText : MonoBehaviour {

    [Header("Wave Counter config:")]
    public string WaveText = "WAVE: ";
    public GUIStyle WaveTextpropeties;
    public int WavePosX = 10;
    public int WavePosY = 10;
    public int WaveSizeW = 150;
    public int WaveSizeH = 20;
    [Header("Wave Cooldown:")]
    public string WaveCText = "NEXT WAVE: ";
    public GUIStyle WaveCooldownpropeties;
    public int WaveCPosX = 10;
    public int WaveCPosY = 10;
    public int WaveCSizeW = 150;
    public int WaveCSizeH = 20;
    [Header("Kill Counter config:")]
    public string KillText = "KILL COUNTER: ";
    public GUIStyle KillTextpropeties;
    public int KillPosX = 10;
    public int KillPosY = 10;
    public int KillSizeW = 150;
    public int KillSizeH = 20;
    [Header("Currency config:")]
    public string CurrText = "Currency: ";
    public GUIStyle CurrTextpropeties;
    public int StartCurrency = 80;
    public int CurrPosX = 10;
    public int CurrPosY = 10;
    public int CurrSizeW = 150;
    public int CurrSizeH = 20;
    [HideInInspector]
    public static int KillCounter = 0;
    public static int WaveCounter = 0;
    public static int Currency;


    // Use this for initialization
    void Start () 
    {
        WaveCounter = 0;
        KillCounter = 0;
        Currency = StartCurrency;

    }

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(WavePosX, WavePosY, WaveSizeW, WaveSizeH), WaveText + WaveCounter, WaveTextpropeties);
    //    GUI.Label(new Rect(WaveCPosX, WaveCPosY, WaveCSizeW, WaveCSizeH), WaveCText + Mathf.Round(WaveHandler.cooldown), WaveCooldownpropeties);
    //    GUI.Label(new Rect(KillPosX, KillPosY, KillSizeW, KillSizeH), KillText + KillCounter, KillTextpropeties);
    //    GUI.Label(new Rect(CurrPosX, CurrPosY, CurrSizeW, CurrSizeH), CurrText + Currency, CurrTextpropeties);
    //}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
