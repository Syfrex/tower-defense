﻿using UnityEngine;

public class Bullet : MonoBehaviour {

    private Transform target;
    public GameObject BulletImpactEffect;
    public GameObject parentobject;
    [HideInInspector]
    public bool Explode = false;


    public void Seek(Transform p_target)
    {
        target = p_target;
    }


	// Update is called once per frame
	void Update () 
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distancePerFrame = parentobject.GetComponent<Turret>().BulletSpeed * Time.deltaTime;

        if(dir.magnitude <= distancePerFrame)
        {
            if(target.gameObject.name == "Bomber")
            { 
            HitTarget();
            }
            else if (target.gameObject.name == "Boss")
            {
                HitTargetBoss();
            }
            return;
        }

        transform.Translate(dir.normalized * distancePerFrame, Space.World);
	
	}
    void HitTarget()
    {
        Explode = true;
        GameObject effectIns = (GameObject)Instantiate(BulletImpactEffect, new Vector3(transform.position.x, transform.position.y, -3), transform.rotation);
        Destroy(effectIns,1f);
        Destroy(gameObject);
        target.GetComponent<Enemy>().CurrentHealth -= parentobject.GetComponent<Turret>().Damage;
        if (parentobject.GetComponent<Turret>().SlowPercent > 0)
        {
            target.GetComponent<Enemy>().speed = parentobject.GetComponent<Turret>().SlowPercent * target.GetComponent<Enemy>().StartSpeed;
            target.GetComponent<Enemy>().SlowCooldown = parentobject.GetComponent<Turret>().SlowDuration;
        }
    }
    void HitTargetBoss()
    {
        Explode = true;
        GameObject effectIns = (GameObject)Instantiate(BulletImpactEffect, new Vector3(transform.position.x, transform.position.y, -3), transform.rotation);
        Destroy(effectIns, 1f);
        Destroy(gameObject);
        target.GetComponent<BossScript>().CurrentHealth -= parentobject.GetComponent<Turret>().Damage;
        if (parentobject.GetComponent<Turret>().SlowPercent > 0)
        {
            target.GetComponent<BossScript>().speed = parentobject.GetComponent<Turret>().SlowPercent * target.GetComponent<BossScript>().StartSpeed;
            target.GetComponent<BossScript>().SlowCooldown = parentobject.GetComponent<Turret>().SlowDuration;
        }
    }
}
