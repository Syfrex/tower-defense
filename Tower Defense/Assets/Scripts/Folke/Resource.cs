﻿using UnityEngine;
using System.Collections;

public class Resource : MonoBehaviour
{
    Vector2 RandomDir;
    private Vector3 Size;
    private float Speed = 10.0f;
    public int diamondValue = 10;
    public AudioClip DiamondSound;
    private AudioSource pickdiamondsound;
    private GameObject currencyHUD;
    private bool GoToTheHUD = false;
    void Start()
    {
        Speed = ResourceSpawn.Speed;
        RandomDir.x = Random.Range(-4,4);
        RandomDir.y = Random.Range(4, -4);
        Size = this.transform.localScale / 2;
        pickdiamondsound = GetComponent<AudioSource>();
        currencyHUD = GameObject.Find("Currency HUD").transform.GetChild(1).gameObject;
    }
    void OnMouseEnter ()
    {
        //Debug.Log("Mouse is over gold");

        this.transform.localScale += Size;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)) //0 är vänster, 1 höger, 2 scroll
        {
            //Debug.Log("Collected Gold");
            //HUDText.Currency += 20;
            pickdiamondsound.PlayOneShot(DiamondSound);

            HUDCounter.CurrencyCounter += diamondValue;
            //gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            //gameObject.GetComponent<Resource>().enabled = false;
            gameObject.layer = 2;

            GoToTheHUD = true;
        }
    }

    void OnMouseExit()
    {

        this.transform.localScale -= Size;
    }
    void GoToHUD()
    {
        transform.position = Vector3.MoveTowards(transform.position, Camera.main.ScreenToWorldPoint(currencyHUD.transform.position), Time.deltaTime * 10);
        transform.localScale -= new Vector3(0.02f, 0.02f,0);
        if (Vector3.Distance(transform.position, Camera.main.ScreenToWorldPoint(currencyHUD.transform.position)) < 0.1 ||this.transform.localScale.x <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if(GoToTheHUD == true)
        {
            GoToHUD();
        }
        if (RandomDir.x == 0 && RandomDir.y == 0)
        {
            RandomDir.x = Random.Range(-4, 4);
            RandomDir.y = Random.Range(4, -4);
        }

        transform.Translate((new Vector3(RandomDir.x, RandomDir.y, 0) * Speed) * Time.deltaTime);
        if(Speed > 0.0f)
        { 
        Speed -= 0.01f;
        }
        else if (Speed < 0)
        {
            Speed = 0;
        }
    }

}
