﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class Turret : MonoBehaviour {

    [Header("Bullet Propeties: ")]

   public AudioClip BulletSound;
   private AudioSource bulletsound;
   private Transform target;
   public GameObject bullet;
   public Transform bulletSpawn;
   public float BulletSpeed = 50;
   //public Transform startsize;


   [HideInInspector]
   public enum TypeOfTurret { DMG, SLOW, SNIPE };
    [HideInInspector]   
    public TypeOfTurret TurretType;
    [HideInInspector]
   public int DMG;
    [HideInInspector]
   public int SLOW;
    [HideInInspector]
    public int SNIPE;

   [Header("Turret Propeties: ")]
    //public GameObject TurretVision;

 //   [HideInInspector]
    public float Range;// = 3.5f;
  //  [HideInInspector]
    public float Damage = 3.0f;
   // [HideInInspector]
    public float turnSpeed = 10f;
   // [HideInInspector]
    public float BulletsPerSecond = 1f;

    public int SoundPerFrame = 1;

    [Header("Slow: ")]
  //  [HideInInspector]
    public float SlowPercent = 50f;
    //[HideInInspector]
    public float SlowDuration = 2f;

    

    private float fireCountdown = 0f;


    private Animator animation;
    private float distancetoEnemy;
    private float enemyclosetomine;
    private int PlayCurrentFrame = 0;
    //private bool activeTurretVision = false;
    //private float closeturretvision = 2f;

    void OnDrawInspector()
    {

    }
	void Start () 
    {
       // startsize = transform;
        animation = this.transform.GetComponent<Animator>();
        bulletsound = GetComponent<AudioSource>();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        SlowPercent = SlowPercent / 100;
        animation.SetBool("Shoot", false);
        InvokeRepeating("CheckSize", 0f, 1f);
       // animation.speed = (1 / BulletsPerSecond);

	}
    void CheckSize() //failsafe in case the size turns out bigger than it should 
    {
        if(this.transform.localScale.x < 0.1)
        {
            this.transform.localScale = new Vector3(0.33f, 0.33f, 0.33f);
        }
        else
        {
            CancelInvoke("CheckSize");
        }
    }
    //void OnMouseOver() //In some version, we used this to check the range of the turrets but we removed it due to request from our audience
    //{
        //if (Input.GetMouseButtonDown(0) || Input.touchCount == 1)
        //{
        //    TurretVision.SetActive(true);
        //    closeturretvision = 2;
        //    activeTurretVision = true;
        //}
    //}
    //void OnMouseExit()
    //{
    //    TurretVision.SetActive(false);
    //}

    void UpdateTarget()
    {
       // rayhit = Physics2D.Raycast(transform.position, )


        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float ShortestDistance = Mathf.Infinity;
        float priotarget = Mathf.Infinity;
        float minetarget = Mathf.Infinity;
        GameObject NearestEnemy = null;
        GameObject EnemyClosertoMine = null;
        foreach (GameObject enemy in enemies)
        {
            distancetoEnemy = Vector3.Distance(new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), new Vector3(enemy.transform.position.x, enemy.transform.position.y,0));    
            if(distancetoEnemy < ShortestDistance)
            {
                ShortestDistance = distancetoEnemy;
                NearestEnemy = enemy;
            }

          if(enemy.gameObject.name == "Bomber")
                enemyclosetomine = Vector3.Distance(new Vector3(enemy.GetComponent<Enemy>().Target.transform.position.x, enemy.GetComponent<Enemy>().Target.transform.position.y, 0), new Vector3(enemy.transform.position.x, enemy.transform.position.y, 0));
          else if (enemy.gameObject.name == "Boss")
              enemyclosetomine = Vector3.Distance(new Vector3(enemy.GetComponent<BossScript>().Target.transform.position.x, enemy.GetComponent<BossScript>().Target.transform.position.y, 0), new Vector3(enemy.transform.position.x, enemy.transform.position.y, 0));
 
            if(enemyclosetomine < priotarget) //Always target the one closest to the base not closest to the turret
                {
                    priotarget = enemyclosetomine;
                    EnemyClosertoMine = enemy;
                    minetarget = Vector3.Distance(new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), new Vector3(EnemyClosertoMine.transform.position.x, EnemyClosertoMine.transform.position.y, 0));    
                }
            
            
        
        }

        if(EnemyClosertoMine != null && minetarget <= Range)
        {
            target = EnemyClosertoMine.transform;
        }

        else if(NearestEnemy != null && ShortestDistance <= Range)
        {
            target = NearestEnemy.transform;
        }
        else
        {
           
            target = null;
        }

    }

    void Update()
    {
        //if(transform.localScale != startsize.localScale)
        //{
        //    transform.localScale = startsize.localScale;
        //}
        //if(activeTurretVision == true && closeturretvision <= 0)
        //{
        //    TurretVision.SetActive(false);
        //    activeTurretVision = false;

        //}
        //else if (closeturretvision > 0)
        //{
        //    closeturretvision -= Time.deltaTime;
        //}
        if (target == null) //In case of not having any target, do nothing.
        {
            return;
        }
        else
            Debug.DrawLine(this.transform.position, target.position);
        
      /* Vector3 _cross = Vector3.Cross(new Vector3(0, 0, 270), target.position -(transform.position));
      // // float direct = ((target.position.y - Camera.main.ScreenToWorldPoint(transform.position).y) / (target.position.x - Camera.main.ScreenToWorldPoint(transform.position).x));

        float dir = _cross.z > 0 ? -1 : 1;
        //  Debug.Log("x " +_cross.z + " y " + _cross.y + " z " + _cross.z );

        transform.Rotate(Vector3.forward * dir * turnSpeed * 10f, Space.World); */
        //Quaternion lookRotate = Quaternion.LookRotation(dir);
        //Vector3 rotation = lookRotate.eulerAngles;//Quaternion.Lerp(this.gameObject.transform.rotation, lookRotate, Time.deltaTime * turnSpeed).eulerAngles;
     
        Vector3 dir = target.position - (transform.position);//transform.position;
         float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        this.transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * turnSpeed);
        //this.transform.rotation = Quaternion.Euler(0f, 0f, rotation.z);
        
        ////transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(target.position, transform.position), 1); // this.transform.Rotate(Vector3.forward, Vector3.Angle(transform.position, target.position));
        //Debug.Log(Camera.main.ScreenToWorldPoint(transform.position));
        PlayCurrentFrame--;

        if (fireCountdown <= 0)
        {
            //if(typeOfTurret == Draggable.TurretType.FASTDMG)
            //{ 
            if (PlayCurrentFrame <= 0)
            {
                bulletsound.PlayOneShot(BulletSound);
                PlayCurrentFrame = SoundPerFrame;
            }
            ShootDMG();
            fireCountdown = 1f / BulletsPerSecond;


            //}
            //else if (typeOfTurret == Draggable.TurretType.SLOW)
            //{
            //    ShootSLOW();
            //    fireCountdown = 1f / fireRate;
            //}
            //else if (typeOfTurret == Draggable.TurretType.BURSTDMG)
            //{
            //    ShootBURST();
            //    fireCountdown = 1f / fireRate;
            //}

        }
        else
        {
            fireCountdown -= Time.deltaTime;
        }
        //if (animation.GetAnimatorTransitionInfo(0).IsName("Shoot"))
        //{
        //    Debug.Log("Test");
        //}
      
 
      //  Debug.Log(animation.GetBool(distancetoEnemy + " + " + Range));
        //if (animation.GetBool("Shoot") == true && distancetoEnemy > Range)
        //{
        //    Debug.Log("IsthisTrue");
        //}
        

    }

    void ShootDMG()
    {
        GameObject bulletGameobject = (GameObject)Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation); //Not very optimized. Creates a bullet each time a turret shoot 
        //bulletGameobject.transform.parent = this.transform;
        Bullet bulletGo = bulletGameobject.GetComponent<Bullet>();
        bulletGo.parentobject = this.transform.gameObject;
        animation.SetTrigger("ShootBullet");
        if (animation.GetBool("Shoot") == false)
            animation.SetBool("Shoot", true);
        if(bulletGo != null)
        {
            bulletGo.Seek(target);
        }

    }
    //void ShootSLOW()
    //{
    //    GameObject bulletGameobject = (GameObject)Instantiate(bullet, bulletSpawn.position/*Camera.main.ScreenToWorldPoint(new Vector3(bulletSpawn.position.x, bulletSpawn.position.y, 0))*/, bulletSpawn.rotation);
    //    Bullet bulletGo = bulletGameobject.GetComponent<Bullet>();
    //    if (bulletGo != null)
    //    {
    //        bulletGo.Seek(target);
    //    }
    //}
    //void ShootBURST()
    //{
    //    GameObject bulletGameobject = (GameObject)Instantiate(bullet, bulletSpawn.position/*Camera.main.ScreenToWorldPoint(new Vector3(bulletSpawn.position.x, bulletSpawn.position.y, 0))*/, bulletSpawn.rotation);
    //    Bullet bulletGo = bulletGameobject.GetComponent<Bullet>();
    //    if (bulletGo != null)
    //    {
    //        bulletGo.Seek(target);
    //    }
    //}


    void OnDrawGizmosSelected () //For level designer (drawing lines in the inspector insted of gamescreen)
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(this.transform.position, Range);
    }
}





//using UnityEngine;
//using System.Collections;

//public class Turret : MonoBehaviour {


//    private GameObject[] Enemies;
//    GameObject Target;
//    // Use this for initialization
//    void Start () 
//    {
//        Enemies = GameObject.FindGameObjectsWithTag("Enemy");
//        if(Enemies[0] != null)
//        {
//            Target = Enemies[0];
//        }
//    }
	
//    // Update is called once per frame
//    void Update () 
//    {
//        for (int i = 0; i < Enemies.Length; i++)
//        {
//            if(Enemies[i] != null)
//            { 
//            if (Vector3.Distance(this.gameObject.transform.position, Enemies[i].transform.position) < Vector3.Distance(this.gameObject.transform.position, Target.transform.position))
//            {
//                Target = Enemies[i];
//            }
//            }
//        }
//        Debug.DrawLine(this.transform.position, Target.transform.position);
//        //transform.LookAt(new Vector3(Target.transform.position.y,Target.transform.position.x,0));
//    }
//}
