﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {

    [Header ("---Enemy Spawner Stats---")]
   public float TimeBetweenSpawn = 1.0f;
   public int WhatWaveToActivate = 0;
   public int MaxSpawnOfEnemies = 10;
   public Transform[] wayPoints = null;
   private int AmountOfEnemys;
   public bool RepeatableWaves = false;
   public float WhenOnWaveToActive = 0;
   [HideInInspector]
   public float wavetimetoactive;

   [Header("---Enemy Stats---")]
   public bool IsItAnArnold = false;
   public float Speed = 0.1f;
    public GameObject EnemyPrefab;
   private int SpawnedEnemies = 0;

   private GameObject wavehandle;
   private GameObject EnemieSpawned;
	// Use this for initialization
	void Start () 
    {
        wavehandle = GameObject.Find("WaveHandler");
        wavetimetoactive = WhenOnWaveToActive;
        SpawnedEnemies = 0;
        InvokeRepeating("EnemySpawnPoint", 0 , TimeBetweenSpawn);
    }

    void EnemySpawnPoint()
    {
        if (WhenOnWaveToActive <= WaveHandler.WaveTimer)
        { 
        if (RepeatableWaves == true)
        { 
            if (WhatWaveToActivate <= HUDCounter.WaveCounter /*HUDText.WaveCounter*/ && AmountOfEnemys < MaxSpawnOfEnemies)
            {
                EnemieSpawned = (GameObject)Instantiate(EnemyPrefab, transform.position, transform.rotation);
                EnemieSpawned.GetComponent<Enemy>().speed = Speed;
                EnemieSpawned.transform.parent = this.transform;
                AmountOfEnemys += 1;
                SpawnedEnemies += 1;
                if(WaveHandler.cooldown != wavehandle.GetComponent<WaveHandler>().CooldownTimer)
                {
                    WaveHandler.cooldown = wavehandle.GetComponent<WaveHandler>().CooldownTimer;
                }
            }
        }

        if (RepeatableWaves == false)
        {
            if (WhatWaveToActivate == HUDCounter.WaveCounter /*HUDText.WaveCounter*/ && AmountOfEnemys < MaxSpawnOfEnemies)
            {
                EnemieSpawned = (GameObject)Instantiate(EnemyPrefab, transform.position, transform.rotation);
                EnemieSpawned.GetComponent<Enemy>().speed = Speed;
                EnemieSpawned.transform.parent = this.transform;
                AmountOfEnemys += 1;
                SpawnedEnemies += 1;
                if(WaveHandler.cooldown != wavehandle.GetComponent<WaveHandler>().CooldownTimer)
                {
                    WaveHandler.cooldown = wavehandle.GetComponent<WaveHandler>().CooldownTimer;
                }
            }
        }
        }

    }
	
	// Update is called once per frame
	void Update () 
    {
        if (WaveHandler.cooldown <= 0 && AmountOfEnemys != 0)
        {
            AmountOfEnemys = 0;
            MaxSpawnOfEnemies += 1;
        }

    }
}
