﻿using UnityEngine;
using System.Collections;

public class ExplosiveBullet : MonoBehaviour {

    public float aoeDamage = 2;
    public GameObject AoE;
	// Use this for initialization
	void Start () 
    {
	
	}
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.name == "Bomber")
        {
            coll.GetComponent<Enemy>().CurrentHealth -= aoeDamage;
            // Destroy(this.gameObject);
        }
        else if (coll.transform.name == "Boss")
        {
            coll.GetComponent<BossScript>().CurrentHealth -= aoeDamage;
            // Destroy(this.gameObject);
        }
    }

}
