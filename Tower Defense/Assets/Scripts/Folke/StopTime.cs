﻿using UnityEngine;
using System.Collections;

public class StopTime : MonoBehaviour
{
    public void ResumeTime()
    {
        Time.timeScale = 1.0f;
    }

    public void PauseTime()
    {
        Time.timeScale = 0.0f;
    }
}
