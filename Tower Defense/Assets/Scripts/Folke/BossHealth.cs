﻿using UnityEngine;
using System.Collections;

public class BossHealth : MonoBehaviour {
    // Use this for initialization
    private float scalesize;
    private float startscale;
    void Start()
    {
        scalesize = transform.parent.GetComponent<BossScript>().MaxHealth / transform.parent.GetComponent<BossScript>().CurrentHealth;
        startscale = this.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(transform.parent.transform.position.x, transform.parent.transform.position.x, this.transform.position.z);
        scalesize = transform.parent.GetComponent<BossScript>().CurrentHealth / transform.parent.GetComponent<BossScript>().MaxHealth;
        // Debug.Log(scalesize);
        this.gameObject.transform.localScale = new Vector2(startscale * scalesize, this.transform.localScale.y);
    }
}
