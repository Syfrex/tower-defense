﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	// Use this for initialization
    private float scalesize;
    private float startscale;
	void Start () 
    {
        scalesize = transform.parent.GetComponent<Enemy>().MaxHealth / transform.parent.GetComponent<Enemy>().CurrentHealth;
        startscale = this.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //transform.position = new Vector3(transform.parent.transform.position.x, transform.parent.transform.position.x, this.transform.position.z);
        scalesize = transform.parent.GetComponent<Enemy>().CurrentHealth / transform.parent.GetComponent<Enemy>().MaxHealth;
       // Debug.Log(scalesize);
        this.gameObject.transform.localScale = new Vector2(startscale * scalesize , this.transform.localScale.y);
	}
}
