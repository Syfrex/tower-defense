﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Loading : MonoBehaviour
{

    AsyncOperation ao;
    public GameObject LoadingScreen;
    public Slider ProgressBar;

    public bool LoadingBar = false;
    public string leveltoload = "FifthAlpha";
    public void LoadingScene()
    {
        LoadingScreen.SetActive(true);
        ProgressBar.gameObject.SetActive(true);
        if (!LoadingBar)
        {
            StartCoroutine(LoadLevelWithRealProgress());
        }
        else
        {

        }
    }
    IEnumerator LoadLevelWithRealProgress()
    {
        yield return new WaitForSeconds(1);

        ao = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(leveltoload);
        ao.allowSceneActivation = false;


        while (!ao.isDone)
        {
            ProgressBar.value = ao.progress;
            if (ao.progress == 0.9f)
            {
                ProgressBar.value = 1.0f;
                ao.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
