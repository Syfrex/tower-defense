﻿using UnityEngine;
using System.Collections;

public class Driller : MonoBehaviour {

    public float SpawnRate = 1.0f;
    public float WhenToActive = 0f;
    public float SpeedWhenSpawn = 1.0f;
    public static float Speed = 0.1f;
    public GameObject GoldPrefab;
    private GameObject GoldSpawned;
    // Use this for initialization
    void Start()
    {
        Speed = SpeedWhenSpawn;
        InvokeRepeating("GoldSpawnPoint", WhenToActive, SpawnRate);
    }

    void GoldSpawnPoint()
    {
        GoldSpawned = (GameObject)Instantiate(GoldPrefab, transform.position, transform.rotation);
        //GoldSpawned.transform.parent = this.transform;
    }
}
