﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour
{

    [HideInInspector]
    public Transform Target;
    [Header("Target: ")]
    public GameObject[] DiamondminePrefab;
    private Transform[] waypoints;
    private int CurrentWaypoint;
    private float distancetomine;
    [Header("Propeties: ")]

    public float Damage = 0.02f;
    public float AttacksPerSecond = 1;
    Vector2 StartPos;
    private bool uglysolution = false;
    public float CurrentHealth = 10f;
    [HideInInspector]
    public float MaxHealth;
    Vector3 direction;
    [Header("Effects: ")]
    public GameObject Tombstone;
    [HideInInspector]
    public float speed = 0.5f;
    public float StartSpeed;
    public bool Chilleffect = false;
    public float SlowCooldown;
    [Header("Diamond Spawn: ")]
    public GameObject DiamondPrefab = null;
    public int AmountofDiamonds = 3;

    private Animator animat;
    private float fireCountdown = 0f;
    void Start()
    {
        this.transform.name = "Boss";
        animat = this.transform.GetComponent<Animator>();
        MaxHealth = CurrentHealth;
        StartSpeed = speed;
        waypoints = transform.parent.GetComponent<BossSpawn>().wayPoints;
        WaveHandler.EnemiesAlive += 1;
        StartPos = this.transform.position;
        DiamondminePrefab = GameObject.FindGameObjectsWithTag("CandyPlanet");
        float ShortestDistance = Mathf.Infinity;
        if (waypoints.Length > 0)
        {
            Target = waypoints[0];
            direction = waypoints[0].position - transform.position;
        }
        else
        {
            foreach (GameObject CandyPlanet in DiamondminePrefab)
            {
                distancetomine = Vector3.Distance(new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), new Vector3(CandyPlanet.transform.position.x, CandyPlanet.transform.position.y, 0));
                if (distancetomine < ShortestDistance)
                {
                    ShortestDistance = distancetomine;
                    Target = CandyPlanet.transform;
                    direction = Target.transform.position - transform.position;
                    uglysolution = true;
                }
            }
        }
        CurrentWaypoint = 0;
        InvokeRepeating("CheckHealth", 0f, 0.1f);
        InvokeRepeating("CheckSpeed", 0f, 0.1f);
    }
    void CheckSpeed()
    {
        if (SlowCooldown > 0)
        {
            SlowCooldown -= 0.1f;
        }
        else if(SlowCooldown <= 0 && Vector3.Distance(transform.position, Target.position) > 3.5f)
        {
            speed = StartSpeed;
        }
    }

    void CheckHealth()
    {
        if (CurrentHealth <= 0)
        {
            WaveHandler.EnemiesAlive -= 1;
            GameObject tombstone = (GameObject)Instantiate(Tombstone, new Vector3(transform.position.x, transform.position.y, -3), Tombstone.transform.rotation);
            HUDText.KillCounter++;
            if(AmountofDiamonds != 0)
            {
                for(int i = 0; i < AmountofDiamonds; i++)
                {
                    Instantiate(DiamondPrefab, transform.position, DiamondPrefab.transform.rotation);
                }
            }
            Destroy(tombstone, 4f);
            Destroy(gameObject);
        }
    }
    void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, Target.position, Time.deltaTime * speed);
        if (DiamondminePrefab == null)
        {
            Destroy(gameObject);
            return;
        }

        if (Vector3.Distance(transform.position, Target.position) < 3.5f)
        {
            if (uglysolution == false)
                Patrol();
            else
            {
                if (fireCountdown <= 0)
                {
                    HitTarget();
                    fireCountdown = 1f / AttacksPerSecond;
                }
                else
                {
                    fireCountdown -= Time.deltaTime;
                }
            }
            return;
        }
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        this.transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime);
    }

    void Patrol()
    {
        CurrentWaypoint += 1;
        if (CurrentWaypoint < waypoints.Length) //If the waypoint exist
        {
            direction = waypoints[CurrentWaypoint].position - transform.position;
            Target = waypoints[CurrentWaypoint];
        }

        else //if the waypoint don't exist, the last point is the base
        {
            float ShortestDistance = Mathf.Infinity;
            foreach (GameObject CandyPlanet in DiamondminePrefab)
            {
                distancetomine = Vector3.Distance(new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, 0), new Vector3(CandyPlanet.transform.position.x, CandyPlanet.transform.position.y, 0));
                if (distancetomine < ShortestDistance)
                {
                    ShortestDistance = distancetomine;
                    Target = CandyPlanet.transform;
                    direction = Target.transform.position - transform.position;
                    uglysolution = true;
                }
            }
        }
    }

    void HitTarget()
    {
        if(animat.GetBool("Hacking") == false)
        { 
        animat.SetBool("Hacking", true);
        }
        WaveHandler.EnemiesAlive -= 1;
        MineScript.Health -= Damage;
        speed = 0;
    }
}
