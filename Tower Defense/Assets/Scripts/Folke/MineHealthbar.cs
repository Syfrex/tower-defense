﻿using UnityEngine;
using System.Collections;

public class MineHealthbar : MonoBehaviour {
    public Material GreenColor;
    public Material YellowColor;
    public Material RedColor;

	// Use this for initialization
	void Start () {
        InvokeRepeating("Checkhealth", 0.0f, 0.3f);
	
	}
	void Checkhealth()
    {
        if (MineScript.Health >= 7)
        {
            gameObject.GetComponent<SpriteRenderer>().color = GreenColor.color;
        }
        else if (MineScript.Health > 4)
        {
            gameObject.GetComponent<SpriteRenderer>().color = YellowColor.color;

        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = RedColor.color;

        }
    }
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.localScale = new Vector2(MineScript.Health/4, this.transform.localScale.y);
	
	}
}
