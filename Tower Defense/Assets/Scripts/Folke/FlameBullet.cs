﻿using UnityEngine;

public class FlameBullet : MonoBehaviour {

    private Transform target;
    [HideInInspector]
    public bool Explode = false;


    public void Seek(Transform p_target)
    {
        target = p_target;
    }


	// Update is called once per frame
	void Update () 
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distancePerFrame = transform.parent.GetComponent<FlameThrower>().BulletSpeed * Time.deltaTime;

        if(dir.magnitude <= distancePerFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distancePerFrame, Space.World);
	
	}
    void HitTarget()
    {
        Explode = true;
        Destroy(gameObject);
        //target.GetComponent<Enemy>().CurrentHealth -= transform.parent.GetComponent<FlameThrower>().Damage;
      
    }
}
