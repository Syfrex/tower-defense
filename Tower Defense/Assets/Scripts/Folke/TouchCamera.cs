﻿using UnityEngine;
using System.Collections;

public class TouchCamera : MonoBehaviour {

    public float speed = 0.1f; // speed of camera
    private float margin = 1.0f; // amount of movement
    private float zoom = 1.0f; // zoom speed
    public float zoom_out = 20; // minimum zoom for camera
    public float zoom_in = 3; // maximum zoom for the camera
    public static bool isShopping = false;
    public float rubberband = 5;
    Vector2 fingerStartPoistion;

    public float max_xBounds = 100; // the bounds from center xBounds and -xBounds will be the limit
    public float min_xBounds = -100; // the bounds from center xBounds and -xBounds will be the limit
    public float max_yBounds = 100; // the bounds from center zBounds and -zBounds will be the limit
    public float min_yBounds = -100; // the bounds from center zBounds and -zBounds will be the limit

    private GameObject wavecounter;
    private GameObject wavecounterafterten;
  
    void Update()
    {
        if(gameObject.transform.GetChild(0) != null)
        { 
            if(WaveHandler.cooldown < 5)
            {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject.SetActive(true);
                if(HUDCounter.WaveCounter >= 9)
                {
                    gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject.SetActive(true);
                    wavecounterafterten = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter + 1).gameObject;
                }
                //if (HUDCounter.WaveCounter == 9)
                //{
                //    gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject.SetActive(true);
                //    wavecounterafterten = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter - HUDCounter.WaveCounter).gameObject;

                //}
            wavecounter = gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject;
            }
            else
            {
            gameObject.transform.GetChild(0).gameObject.transform.GetChild(HUDCounter.WaveCounter).gameObject.SetActive(false);
            if(wavecounter == isActiveAndEnabled)
            {
                wavecounter.SetActive(false);
            }
                if(wavecounterafterten == isActiveAndEnabled)
                {
                    wavecounterafterten.SetActive(false);
                }
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

        if(isShopping == false)
        {

        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) // camera movement. touchphace.moved = är det något på touchen som rör sig. 
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            if (this.transform.position.x < (min_xBounds - 0.1))
            {
                this.transform.position = new Vector3(min_xBounds,this.transform.position.y,this.transform.position.z) ;//Set(min_xBounds, 0, 0);
            }
            if (this.transform.position.x > (max_xBounds + 0.1))
            {
                this.transform.position = new Vector3(max_xBounds, this.transform.position.y, this.transform.position.z);//.Set(max_xBounds, 0, 0);
            }
            if (this.transform.position.y < (min_yBounds - 0.1))
            {
                this.transform.position = new Vector3(this.transform.position.x, min_yBounds, this.transform.position.z);//.Set(0,min_yBounds, 0 );
            }
            if (this.transform.position.y > (max_yBounds + 0.1))
            {
                this.transform.position = new Vector3(this.transform.position.x, max_yBounds, this.transform.position.z);//.Set(0, max_yBounds, 0);
            }
            //var LevelCenter = new Vector2(transform.position.x, transform.position.y) + touchDeltaPosition;
            //if (LevelCenter.x < min_xBounds || max_xBounds < LevelCenter.x)
            //{
            //    touchDeltaPosition.x = 0;
            //    Debug.Log("Nu");
            //}

            //if (LevelCenter.y < min_yBounds || max_yBounds < LevelCenter.y)
            //{
            //    touchDeltaPosition.y = 0;
            //    Debug.Log("Nu också");
            //}
            //touchDeltaPosition = ScreenToWorldPoint(touchDeltaPosition);
            transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
         
        }
        if (Input.touchCount == 2) //Zoom function touchCount = hur många fingrar på skärmen
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
            // If the camera is orthographic...
        
                // ... change the orthographic size based on the change in distance between the touches.
            if (Camera.main.orthographicSize < zoom_out && Camera.main.orthographicSize > zoom_in)
                Camera.main.orthographicSize += deltaMagnitudeDiff * speed;

            if (Camera.main.orthographicSize > zoom_out)
            {
                Camera.main.orthographicSize = zoom_out - 0.1f;
            }
            if (Camera.main.orthographicSize < zoom_in)
            {
                Camera.main.orthographicSize = zoom_in + 0.1f;
            }
                // Make sure the orthographic size never drops below zero.
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
      

        }
        }
        if(this.transform.position.x > rubberband && Input.touchCount == 0)
        {
            transform.Translate(-this.transform.position.x * speed, 0, 0);
        }
        else if (this.transform.position.x < -rubberband && Input.touchCount == 0)
        {
            transform.Translate(-this.transform.position.x * speed, 0, 0);
        } 
        if (this.transform.position.y > rubberband && Input.touchCount == 0)
        {
            transform.Translate(0, -this.transform.position.y * speed, 0);
        }
        else if (this.transform.position.y < -rubberband && Input.touchCount == 0)
        {
            transform.Translate(0, -this.transform.position.y * speed, 0);
        }
    }
}
