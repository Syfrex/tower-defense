﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MuteButton : MonoBehaviour {

   public bool isMute = true;
    public GameObject muteON;

	public void SoundVolume () 
    {
        if (isMute == false)
        {
            isMute = true;
            muteON.SetActive(false);
            AudioListener.volume = 1;
        }
        else
        {
            isMute = false;
            muteON.SetActive(true);
            AudioListener.volume = 0;
        }

	}
}
