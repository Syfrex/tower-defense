﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RedirectWaypoints : MonoBehaviour
{
    public bool overRideNextWaypoint;
    public bool overRideLastWaypoint;

    public Transform nextWaypoint;
    public Transform lastWaypoint;
    [SerializeField]
    private GameObject overrideThisWaypoint;

    public int overlaped = 0;

    void Start()
    {
        if(overRideNextWaypoint)
        {
            overrideThisWaypoint.GetComponent<RedirectWaypoints>().overlaped += 1;
            overrideThisWaypoint.GetComponent<RedirectWaypoints>().nextWaypoint = this.transform;
        }
        if (overRideLastWaypoint)
        {
            overrideThisWaypoint.GetComponent<RedirectWaypoints>().overlaped += 1;
            overrideThisWaypoint.GetComponent<RedirectWaypoints>().lastWaypoint = this.transform;
        }
    }

    void Update()
    {
        if(overlaped == 3)
        {
            this.gameObject.SetActive(false);
        }
    }

    public Transform GetNextWayPoint()
    {
        return nextWaypoint;
    }

    public Transform GetLastWayPoint()
    {
        return lastWaypoint;
    }
}
