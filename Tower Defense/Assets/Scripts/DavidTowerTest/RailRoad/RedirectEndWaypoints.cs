﻿using UnityEngine;
using System.Collections;

public class RedirectEndWaypoints : MonoBehaviour
{
    private Transform nextWaypoint;
    private Transform lastWaypoint;
    [SerializeField]
    private GameObject overrideThisNextWaypoint;
    [SerializeField]
    private GameObject overrideThisLastWaypoint;
    [SerializeField]
    private GameObject deactivateOriginWaypoint;

    
    void Start()
    {
        overrideThisNextWaypoint.GetComponent<RedirectWaypoints>().nextWaypoint = this.transform;
        overrideThisLastWaypoint.GetComponent<RedirectWaypoints>().lastWaypoint = this.transform;

        nextWaypoint = overrideThisLastWaypoint.transform;
        lastWaypoint = overrideThisNextWaypoint.transform;

        deactivateOriginWaypoint.SetActive(false);
    }

    public Transform GetNextWayPoint()
    {
        return nextWaypoint;
    }

    public Transform GetLastWayPoint()
    {
        return lastWaypoint;
    }
}
