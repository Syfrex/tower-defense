﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ActivateExtension : MonoBehaviour
{
    public GameObject Child;
    public GameObject extention;
    public GameObject deactivateRail;
    public GameObject trainPrefab;

    void Start()
    {
        trainPrefab = GameObject.Find("Old Sprite Node Train");
    }

    void Update()
    {
        if (GetComponent<BoxCollider2D>().bounds.Contains(trainPrefab.transform.position))
        {
            GetComponent<SlotScript>().trainIsWithin = true;
        }
        else
        {
            GetComponent<SlotScript>().trainIsWithin = false;
        }
    }

    public void ActivatePointExtension()
    {
        Child.SetActive(true);
        extention.SetActive(true);

       deactivateRail.SetActive(false);
       this.gameObject.SetActive(false);
    }
}