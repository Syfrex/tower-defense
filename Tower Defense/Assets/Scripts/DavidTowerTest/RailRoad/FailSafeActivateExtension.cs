﻿using UnityEngine;
using System.Collections;

public class FailSafeActivateExtension : MonoBehaviour
{
    public GameObject extantion1;
    public GameObject extantion2;
    public GameObject extantion3;

    private bool matches;

    void Update()
    {
        if (extantion1.activeInHierarchy && extantion2.activeInHierarchy)
        {
            matches = true;
        }
    }

    public void CheckVisibility(bool ok)
    {
        if (matches == true)
        {
            extantion3.gameObject.SetActive(ok);
        }
    }
}
