﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ActivateEndExtension : MonoBehaviour
{
    public GameObject Child;
    public GameObject extention;
    public GameObject deactivateRail1;
    public GameObject deactivateRail2;
    public GameObject trainPrefab;

    void Start()
    {
        trainPrefab = GameObject.Find("Old Sprite Node Train");
    }

    void Update()
    {
        if (GetComponent<BoxCollider2D>().bounds.Contains(trainPrefab.transform.position))
        {
            GetComponent<SlotScript>().trainIsWithin = true;
        }
        else
        {
            GetComponent<SlotScript>().trainIsWithin = false;
        }
    }

    public void ActivateEndPointExtension()
    {
        Child.SetActive(true);
        extention.SetActive(true);

        deactivateRail1.SetActive(false);
        deactivateRail2.SetActive(false);
        this.transform.parent.gameObject.SetActive(false);
    }
}