﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CartScript : MonoBehaviour
{
        //FollowParent variabels
    public Transform parentTargetPos;
    public float cartSpeed;
    private Transform cartPos;
    private Vector2 realPos;

        //NodeTravel Variabels
    //private Transform[] wayPointList;
    private List<Transform> wayPointsList;
    private int currentWayPoint = 0;
    private Transform targetWayPoint;
    private int listSize;
    
    void Start ()
    {
        // FollowParent Variables
        cartPos = this.transform;
        realPos = cartPos.localPosition;
        parentTargetPos = cartPos.parent;
     
        // Node Travel
        wayPointsList = TrainScript.Instance.WayPointsList();
        currentWayPoint = cartPos.parent.parent.GetComponent<FUCKMESCRIPT>().currentNode;
        if (currentWayPoint == -1)
            currentWayPoint = 3;

        listSize = wayPointsList.Count; //.Length;

        //General Variabels
        cartSpeed = TrainScript.currentTrainSpeed;
        cartPos.parent = null;
    }

    void LateUpdate ()
    {
        //check if we have somewere to go
        if (currentWayPoint < this.wayPointsList.Count/*.Length*/)
        {
            if (targetWayPoint == null)
                targetWayPoint = wayPointsList[currentWayPoint];
            if (cartSpeed > 0)
            {
                NodeTravel();
                if (currentWayPoint == listSize - 1)
                    currentWayPoint = -1;
            }
        }
    }

    void FollowParent()
    {
        // Get speed values
        cartSpeed = Mathf.Lerp(cartSpeed, TrainScript.currentTrainSpeed, Time.deltaTime);

        // Get pos and move towards it
        Vector2 destPos = parentTargetPos.TransformPoint(realPos);
        cartPos.position = Vector2.LerpUnclamped(cartPos.position, destPos, cartSpeed * Time.deltaTime);

            // Look at target Functions
        Vector3 dirr = parentTargetPos.position - cartPos.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        cartPos.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    void NodeTravel()
    {
        // Get speed values
        cartSpeed = TrainScript.currentTrainSpeed;

        // Move towards node
        cartPos.position = Vector2.MoveTowards(cartPos.position, targetWayPoint.position, cartSpeed * Time.deltaTime);

        // Look at target Functions
        Vector3 dirr = targetWayPoint.position - cartPos.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        cartPos.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        // Should the list loop Yes/No
        if (transform.position == targetWayPoint.position)
        {
            currentWayPoint++;
            targetWayPoint = wayPointsList[currentWayPoint];
            if (currentWayPoint == listSize - 1)
                currentWayPoint = -1;

        }
        this.GetComponent<FUCKMESCRIPT>().currentNode = currentWayPoint;
    }
}
