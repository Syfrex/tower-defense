﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class ExpandWhenHoverOver : MonoBehaviour
{
    private Vector3 size;

    void Start()
    {
        size = this.transform.localScale / 2;
    }

    void OnMouseEnter()
    {
        this.transform.localScale += size;

        Debug.Log("over");
        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedShopPrefab != null)
        {
            Debug.Log("grow");
        }
    }

    void OnMouseExit()
    {
        this.transform.localScale -= size;
    }
}
