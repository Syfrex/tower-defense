﻿using UnityEngine;
using System.Collections;

public class TrainDMGCollider : MonoBehaviour
{
    public GameObject BloodImpactEffect;
    public float damageToBombers;

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Enemy" && other.gameObject.name != "Boss")
        {
            if(other.transform.parent.GetComponent<EnemySpawn>().IsItAnArnold == false && Train_GetDirectionFromNodes.currentSpeed > 0.1f)
            { 
            other.gameObject.GetComponent<Enemy>().CurrentHealth -= damageToBombers;
            GameObject effectIns = (GameObject)Instantiate(BloodImpactEffect, new Vector3(transform.position.x, transform.position.y, -3), transform.rotation);
            Destroy(effectIns, 1f);
            }
        }
    }
}
