﻿using UnityEngine;
using System.Collections;

public class Cart_GetDirectionFromNodes : MonoBehaviour
{
    //public variabels
    public float currentSpeed;
    public Transform targetWayPointForward;
    public Transform targetWayPointReverse;
    
    public bool forward = false;
    public bool reverse = false;

    //private variables
    private Transform thisTransform;
    private Transform CartinFronofMe;
    private float StartDistance;

    void Start()
    {
        thisTransform = this.transform;
        if (thisTransform.parent.parent.tag == "Train")
        {
            targetWayPointForward = thisTransform.parent.parent.GetComponent<Train_GetDirectionFromNodes>().targetWayPointForward;
            targetWayPointReverse = thisTransform.parent.parent.GetComponent<Train_GetDirectionFromNodes>().targetWayPointReverse;
            currentSpeed = Train_GetDirectionFromNodes.currentSpeed;
        }
        if (thisTransform.parent.parent.tag == "Cart")
        {
            targetWayPointForward = thisTransform.parent.parent.GetComponent<Cart_GetDirectionFromNodes>().targetWayPointForward;
            targetWayPointReverse = thisTransform.parent.parent.GetComponent<Cart_GetDirectionFromNodes>().targetWayPointReverse;
            currentSpeed = Train_GetDirectionFromNodes.currentSpeed;
        }
        CartinFronofMe = thisTransform.parent.parent;
        StartDistance = Vector3.Distance(thisTransform.position, CartinFronofMe.position);
        thisTransform.parent = null;
    }

    //void OnTriggerEnter2D(Collider2D coll)
    //{
    //    //Get where to go TO
    //    if (coll.gameObject.tag == "Node")
    //    {
    //        targetWayPointForward = coll.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();

    //        targetWayPointReverse = coll.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
    //    }
    //    if (coll.gameObject.tag == "EndNode")
    //    {
    //        targetWayPointForward = coll.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();

    //        targetWayPointReverse = coll.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
    //    }
    //}

    void Update()
    {
        //currentSpeed = Train_GetDirectionFromNodes.currentSpeed;
        //Debug.Log(Vector3.Distance(thisTransform.position, CartinFronofMe.position));
        // check if we have somewere to Go and then Go
        if (Vector3.Distance(thisTransform.position, CartinFronofMe.position) > StartDistance && currentSpeed < 8)
        {
            currentSpeed += 0.1f;
        }
        if (Vector3.Distance(thisTransform.position, CartinFronofMe.position) < StartDistance)
        {
            currentSpeed -= 0.1f;
        }
        if (Vector3.Distance(thisTransform.position, CartinFronofMe.position) > StartDistance - 0.2f && Vector3.Distance(thisTransform.position, CartinFronofMe.position) < StartDistance + 0.2f)
        {
            currentSpeed = Train_GetDirectionFromNodes.currentSpeed;
        }
        
        
        //else
        //{
        //    currentSpeed = Train_GetDirectionFromNodes.currentSpeed;
        //}

        if (targetWayPointForward != null && targetWayPointReverse != null)
        {
            if (currentSpeed > 0 /*forward*/)
            {
                Forward();
            }
            if (reverse)
            {
                Reverse();
            }
        }
    }

    void Forward()
    {
        // Get speed values
        //currentSpeed = Train_GetDirectionFromNodes.currentSpeed;

        // Move forward towards the target
        thisTransform.position = Vector2.MoveTowards(thisTransform.position, targetWayPointForward.position, currentSpeed * Time.deltaTime);
        // 2D look at target
        Vector3 dirr = targetWayPointForward.position - thisTransform.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        thisTransform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        //Vector3.Distance(this.transform.position, targetWayPointForward)
        if (Vector3.Distance(this.transform.position, targetWayPointForward.position) < 0.1)//(this.transform.position == targetWayPointForward.position)
        {
            if (targetWayPointForward.gameObject.tag == "Node")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
            }
            if (targetWayPointForward.gameObject.tag == "EndNode")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
            }
        }
    }

    void Reverse()
    {
        // Get speed values
        //currentSpeed = Train_GetDirectionFromNodes.currentSpeed;

        //do reverse
        thisTransform.position = Vector2.MoveTowards(thisTransform.position, targetWayPointReverse.position, currentSpeed * Time.deltaTime);
        // 2D look away from target
        Vector3 dirr = thisTransform.position - targetWayPointReverse.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        thisTransform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        if (this.transform.position == targetWayPointReverse.position)
        {
            if (targetWayPointReverse.gameObject.tag == "Node")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
            }
            if (targetWayPointReverse.gameObject.tag == "EndNode")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
            }
        }
    }
}
