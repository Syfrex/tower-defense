﻿using UnityEngine;
using System.Collections;

public class MeleeTrainPlayAnimations : MonoBehaviour
{
    [SerializeField]
    private Animator[] knives;

	void Update ()
    {
        if(Train_GetDirectionFromNodes.currentSpeed /*GetComponentInParent<Cart_GetDirectionFromNodes>().currentSpeed*/ > 0.1f)
        {
            foreach(Animator ex in knives)
            {
                ex.SetBool("Moving", true);
            }
        }

        if(Train_GetDirectionFromNodes.currentSpeed /*GetComponentInParent<Cart_GetDirectionFromNodes>().currentSpeed*/ < 0.1f)
        {
            foreach (Animator ex in knives)
            {
                ex.SetBool("Moving", false);
            }
        }
    }
}
