﻿using UnityEngine;
using System.Collections;

public class GetDirectionFromNode : Singleton<GetDirectionFromNode>
{
    //public variabels
    public static float currentSpeed;
    public static bool alteredSpeed = false;
    public Transform targetWayPointForward;
    public Transform targetWayPointReverse;
    public bool train = false;
    public bool cart = false;
    public bool forward = false;
    public bool reverse = false;
    
    //private variables
    private Transform thisTransform;

    void Start()
    {
        if (train)
        {
            currentSpeed = 2f;
            thisTransform = this.transform;
        }

        if(cart)
        {
            thisTransform = this.transform;

            targetWayPointForward = thisTransform.parent.parent.GetComponent<GetDirectionFromNode>().TargetWayPointForward();
            targetWayPointReverse = thisTransform.parent.parent.GetComponent<GetDirectionFromNode>().TargetWayPointReverse();
            currentSpeed = thisTransform.parent.parent.GetComponent<GetDirectionFromNode>().CurrentSPeed();
            alteredSpeed = thisTransform.parent.parent.GetComponent<GetDirectionFromNode>().AlteredSpeed();

            thisTransform.parent = null;
        }
    }

    void HandleSpeedValues()
    {
        currentSpeed = Mathf.Lerp(currentSpeed, GameManager.Instance.InputMovementSpeed.DetermineSpeed, Time.deltaTime);
    }

    //void OnTriggerEnter2D(Collider2D coll)
    //{
    //    //Get where to go TO
    //    if(coll.gameObject.tag == "Node")
    //    {
    //        targetWayPointForward = coll.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();

    //        targetWayPointReverse = coll.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
    //    }
    //    if(coll.gameObject.tag == "EndNode")
    //    {
    //        targetWayPointForward = coll.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();

    //        targetWayPointReverse = coll.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
    //    }
    //}

    void Update()
    {
        if (alteredSpeed == true)
            HandleSpeedValues();

        // check if we have somewere to Go and then Go
        if (targetWayPointForward != null && targetWayPointReverse != null)
        {
            if (currentSpeed > 0 /*forward*/)
            {
                Forward();
            }
            if (reverse)
            {
                Reverse();
            }
        }
    }

    void Forward()
    {
        // move forward towards the target
        thisTransform.position = Vector2.MoveTowards(thisTransform.position, targetWayPointForward.position, currentSpeed * Time.deltaTime);
        // 2D look at target
        Vector3 dirr = targetWayPointForward.position - thisTransform.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        thisTransform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        if(this.transform.position == targetWayPointForward.position)
        {
            if(targetWayPointForward.gameObject.tag == "Node")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
            }
            if (targetWayPointForward.gameObject.tag == "EndNode")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
            }
        }
    }

    void Reverse()
    {
        //do reverse
        thisTransform.position = Vector2.MoveTowards(thisTransform.position, targetWayPointReverse.position, currentSpeed * Time.deltaTime);

        // 2D look away from target
        Vector3 dirr = thisTransform.position - targetWayPointReverse.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        thisTransform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        if (this.transform.position == targetWayPointReverse.position)
        {
            if (targetWayPointReverse.gameObject.tag == "Node")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectWaypoints>().GetLastWayPoint();
            }
            if (targetWayPointReverse.gameObject.tag == "EndNode")
            {
                targetWayPointForward = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetNextWayPoint();
                targetWayPointReverse = targetWayPointForward.gameObject.GetComponent<RedirectEndWaypoints>().GetLastWayPoint();
            }
        }
    }

    public Transform TargetWayPointForward()
    {
        return targetWayPointForward;
    }

    public Transform TargetWayPointReverse()
    {
        return targetWayPointReverse;
    }

    public float CurrentSPeed()
    {
        return currentSpeed;
    }

    public bool AlteredSpeed()
    {
        return alteredSpeed;
    }
}
