﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TrainScript : Singleton<TrainScript>
{
    //public variables
    // put the points from unity interface
    //public GameObject parentNode;

    //private variables
    public static float currentTrainSpeed;
    public static bool alterTrainSpeed;
    //private Transform[] wayPointList;
    private List<Transform> wayPointsList;

    private int listSize;

    private Transform trainPos;
    private int currentWayPoint = 0;
    private Transform targetWayPoint;


    void OnEnable()
    {
        wayPointsList =  GameManager.Instance.WayPoints();/* .ParentNode().GetComponentsInChildren<Transform>();*/

        currentTrainSpeed = 2f;
        alterTrainSpeed = false;

        listSize = wayPointsList.Count; /*.Length;*/


        trainPos = this.transform;
    }



    void HandleSpeedValues()
    {
        currentTrainSpeed = Mathf.Lerp(currentTrainSpeed, GameManager.Instance.InputMovementSpeed.DetermineSpeed, Time.deltaTime);
    }

    void Update()
    {
        if(alterTrainSpeed == true)
            HandleSpeedValues();

        // check if we have somewere to go
        if (currentWayPoint < wayPointsList.Count /*.Length*/)
        {
            if (targetWayPoint == null)
                targetWayPoint = wayPointsList[currentWayPoint];
            if (currentTrainSpeed > 0)
            {
                Forward();
            }
            if (currentTrainSpeed < 0)
            {
                //Reverse();
            }
        }
    }
    
    void Forward()
    {
            // move towards the target
        trainPos.position = Vector2.MoveTowards(trainPos.position, targetWayPoint.position, currentTrainSpeed * Time.deltaTime);

            // 2D look at target functions
        Vector3 dirr = targetWayPoint.position - trainPos.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        trainPos.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

            // Should the list loop Yes/No
        if (transform.position == targetWayPoint.position)
        {
            currentWayPoint++;
            targetWayPoint = wayPointsList[currentWayPoint];
            if (currentWayPoint == listSize - 1)
                currentWayPoint = -1;


        }
        this.GetComponent<FUCKMESCRIPT>().currentNode = currentWayPoint;

    }

    void Reverse()
    {

    }

    //public Transform[] WayPointList()
    //{
    //    return wayPointList;
    //}

    public List<Transform> WayPointsList()
    {
        return wayPointsList;
    }

    public int CurrentWayPoint()
    {
        return currentWayPoint;
    }

    public Transform TargetWayPoint()
    {
        return targetWayPoint;
    }

    public int ListSize()
    {
        return listSize;
    }
}
