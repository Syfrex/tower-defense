﻿using UnityEngine;
using System.Collections;

public class CircularMovement : MonoBehaviour
{
    public Transform center; //obj to circle
    public float degreesPerSecond = -65.0f; //turning angle
    public float trainSpeed;
    private Vector3 v;

    // Use this for initialization
    void Start()
    {
        v = transform.position - center.position; //distance from center
    }

    // Update is called once per frame
    void Update()
    {
        //Quaterion is a rotation. It does nothing by itself and you cannot assign a Quaternion to a vector3.
        //But if you multiply a Vector3 by a Quaternion, then you the result is a Vector3 rotated by the Queaternion
        v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * trainSpeed, Vector3.forward) * v;
        transform.position = center.position + v;

    }
}
