﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenereateRailSprite : MonoBehaviour
{
    public GameObject spriteToDuplicate;
    private Transform thisStart;
    public Transform endHere;
    private float dist;
    private float size;
    private Vector3 currentPosition;


    void Awake()
    {
        //spriteToDuplicate = this.gameObject;
        thisStart = this.transform;
        dist = Vector3.Distance(thisStart.position, endHere.position);
    }

    void Start ()
    {
        currentPosition = thisStart.transform.position;
        size = spriteToDuplicate.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        size *= spriteToDuplicate.transform.localScale.x;

        //size = 1;
        int alt = (int)(dist / size);
        //Debug.Log("Start of Loop");
        loop(alt);

    }

    void loop(int ihej)
    {
        //Debug.Log("Run Loop");

        for (int i = 0; i <= ihej; i++)
        {
            //Debug.Log("Running " + i);
            GameObject tmpObj = GameObject.Instantiate(spriteToDuplicate, currentPosition, Quaternion.identity) as GameObject;
            tmpObj.transform.SetParent(this.gameObject.transform);
            currentPosition.x += size; //+= (endHere.position - thisStart.position); //new Vector3(1f, 0f, 0f);
            //currentPosition.y += size; //+= (endHere.position - thisStart.position); //new Vector3(1f, 0f, 0f);
        }
    }
}
