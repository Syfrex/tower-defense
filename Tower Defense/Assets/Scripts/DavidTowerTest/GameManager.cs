﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GameManager : Singleton<GameManager>
{
    
    public TowerMenuButton ClickedShopPrefab { get; set; }

    public TrainMenuPedel InputMovementSpeed { get; set; }

    public GameObject parentNode;

    public List<Transform> wayPointList = new List<Transform>();
	
	void Update ()
    {
        HandleEscape();
	}

    public void PickTower(TowerMenuButton shopButton)
    {

        this.ClickedShopPrefab = shopButton;
        ImigeHower.Instance.Activate(shopButton.Sprite);
        shopButton.transform.parent.gameObject.SetActive(false);
    }

    public void BuyPrefab()
    {
        ImigeHower.Instance.Deactivate();
    }

    public void DetermineSpeed(TrainMenuPedel pedelValue)
    {
        this.InputMovementSpeed = pedelValue;
    }

    private void HandleEscape()
    {
        if(Input.GetMouseButtonDown(1))
        {
            ImigeHower.Instance.Deactivate();
        }
    }

    public List<Transform> WayPoints()
    {
        foreach (Transform way in parentNode.GetComponentsInChildren<Transform>())
        {
            wayPointList.Add(way);
        }
        wayPointList.RemoveAt(0);

        int n = wayPointList.Count;
        Debug.Log("antal waypoint " + n);
        
        return wayPointList;
    }

    public void AddNodeToList(Transform newNode)
    {
        wayPointList.Add(newNode);
    }
}