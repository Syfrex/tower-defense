﻿using UnityEngine;
using System.Collections;

public class TurtorialArrowPointer : MonoBehaviour
{
    private SpriteRenderer spriteRender;
    public GameObject turretSlotOnTrain1;
    public GameObject turretSlotOnTrain2;
    public GameObject turretSlotOnTrain3;
    public static bool runOnce;
    void Start ()
    {
        runOnce = false;
        this.spriteRender = GetComponent<SpriteRenderer>();
        spriteRender.enabled = false;
    }

    void Update()
    {
        //if(spriteRender.enabled == true)
        //{
        //    Vector3 dirr = this.transform.position - this.transform.parent.position;
        //    float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        //    this.transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
        //}

        if (turretSlotOnTrain1.tag == "FullSlot" || turretSlotOnTrain2.tag == "FullSlot" || turretSlotOnTrain3.tag == "FullSlot")
            runOnce = true;

        if(runOnce == true)
            GameObject.Destroy(this.gameObject);
    }

    public void EnableArrow()
    {
        spriteRender.enabled = true;
    }
}