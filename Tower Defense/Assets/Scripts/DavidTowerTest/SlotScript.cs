﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SlotScript : Singleton<SlotScript>
{
    public bool isEmpty { get; private set; }
    private Color32 fullColor = new Color32(255, 118, 188, 255);
    private Color32 emptyColor = new Color32(255, 255, 255, 150); //new Color32(96, 225, 90, 255);
    private Color32 cartColor = new Color32(0, 0, 0, 81);
    private Color32 normalColor = new Color32(255, 255, 255, 150);

    private SpriteRenderer spriteRenderer;

    private Transform Turretsize;

    private GameObject mouseHower;
    private GameObject arrowHandler;
    private Vector3 Startdis;
    private GameObject middleslot;
    private GameObject DMGColl;
    private GameObject backsnapturret;
    
    public string whatKindOfSlot;

    private float slotExtendeSize;
    private Vector3 slotOriginalSize;

    public bool trainIsWithin;
    private bool scaleonce = false;

    public static string currentObjectName;
    public static int currentObjectCost;

    private Vector3 parentLocalOGSize;

    void Start()
    {
        parentLocalOGSize = this.transform.parent.transform.localScale;
        mouseHower = GameObject.Find("MouseHower");
        arrowHandler = GameObject.Find("ArrowHandler");
        spriteRenderer = GetComponent<SpriteRenderer>();
        DMGColl = transform.parent.GetChild(0).gameObject;
        isEmpty = true;
        currentObjectName = "";
        currentObjectCost = 0;
        ColorSlot(normalColor);
        trainIsWithin = false;
        slotOriginalSize = this.transform.localScale;// *0.5f;// new Vector3(-0.4f,-0.4f,-0.4f);
        middleslot = GameObject.Find("TurrentSlot (5)");
        InvokeRepeating("CheckIfTurretExist", 0.0f, 5.0f);
        if (whatKindOfSlot == "Cart" || whatKindOfSlot == "Railroad")
            ColorSlot(cartColor);

        Startdis = this.transform.position - DMGColl.transform.position;

    }
    
    void CheckIfTurretExist() //Failsafe no longer in use
    {
        if (this.transform.name == "TurrentSlot (4)" || this.transform.name == "TurrentSlot (5)" || this.transform.name == "TurrentSlot (6)" || this.transform.name == "TurrentSlot (3)")
        {
        if (this.transform.tag == "FullSlot")
        {
            if(this.transform.childCount == 0 )//GetChild(0).gameObject == null)
            {
                Debug.Log("Fixade Turret");
                spriteRenderer.enabled = true;
                ColorSlot(normalColor);
                isEmpty = true;
                this.transform.tag = "EmptyTurretSlot";
                this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
                gameObject.layer = 0;
                
            }
            else
            {
               // CancelInvoke("CheckIfTurretExist");
            }
        }
        }
        //if (this.transform.name == "BackSnap")
        //{
        //    if (this.transform.tag == "FullSlot")
        //    {
        //        Debug.Log("Fixade BackSnap");
        //        if (backsnapturret.gameObject == null)//GetChild(0).gameObject == null)
        //        {
        //            Debug.Log("TestforCart");
        //            spriteRenderer.enabled = true;
        //            ColorSlot(normalColor);
        //            isEmpty = true;
        //            this.transform.tag = "EmptyCartSlot";
        //            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        //            gameObject.layer = 0;

        //        }
        //        else
        //        {
        //            //CancelInvoke("CheckIfTurretExist");
        //        }
        //    }
        //}
    }

    void Update()
    {
        if (TouchCamera.isShopping == true)
        {
            slotExtendeSize = Camera.main.orthographicSize / 5;

            //if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
            //{

            if (currentObjectName == "Turret" && this.transform.tag == "EmptyTurretSlot")
            {
                this.transform.parent.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
            }
            else if (currentObjectName == "Cart" && this.transform.tag == "EmptyCartSlot")
            {
                this.transform.parent.transform.localScale = new Vector3(slotExtendeSize , slotExtendeSize, 0);
            }
            scaleonce = true;


        }
        else if(TouchCamera.isShopping == false && scaleonce == true)
        {
            this.transform.parent.transform.localScale = parentLocalOGSize; // new Vector3(1, 1, 0);

            scaleonce = false;
        }
        //else
        //{
        //    if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
        //    {
        //        //this.transform.position = Startpos;
        //        this.transform.parent.transform.localScale = new Vector3(1, 1, 0);
        //        this.transform.parent.GetChild(0).gameObject.transform.localScale = Vector3.one;


        //        //if (this.transform.name == "TurrentSlot (4)")
        //        //{
        //        //    //this.transform.position = new Vector3((middleslot.transform.position.x) - 2, this.transform.position.y, 0);
        //        //}
        //        //if (this.transform.name == "TurrentSlot (6)")
        //        //{
        //        // //   this.transform.position = new Vector3((middleslot.transform.position.x) + 2, this.transform.position.y, 0);
        //        //}
        //    }
        //    if (currentObjectName == "Cart" && whatKindOfSlot == "Cart" && this.gameObject.tag == "EmptyCartSlot")
        //    {
        //        this.transform.localScale = new Vector3(1, 1, 0);       //Mr.Folke do very good code for very good train 
        //        this.transform.parent.GetChild(0).gameObject.transform.localScale = Vector3.one;

        //    }
        //}
        ///////////////////////Detta är fulkod! Måste reworkas! ///////////////
        if(ReplaceTurret.ReplaceTurretMode == true && this.transform.childCount == 0)
        {
            this.transform.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if(ReplaceTurret.ReplaceTurretMode == false && this.transform.GetComponent<BoxCollider2D>().isActiveAndEnabled == false)
        {
            this.transform.GetComponent<BoxCollider2D>().enabled = true;
        }

        ////////////////////////////////////////////////////////////////////////
        if (transform.childCount > 0 && this.transform.name == "TurrentSlot (4)" || transform.childCount > 0 && this.transform.name == "TurrentSlot (5)" || transform.childCount > 0 && this.transform.name == "TurrentSlot (6)" || transform.childCount > 0 && this.transform.name == "TurrentSlot (3)")
        {
            if (transform.GetChild(0).transform.localScale.x < 0.5f)
            {
                transform.GetChild(0).transform.localScale = new Vector3(0.33f, 0.33f, 0);
            }
        }
        if ( this.transform.parent.transform.tag == "Cart" && TouchCamera.isShopping == false)
        {
            if (transform.parent.transform.localScale.x < 1 || transform.parent.transform.localScale.x > 1)
            {
                transform.parent.transform.localScale = new Vector3(1f, 1f, 0);
            }
        }
    }
    //void Update()
    //{ 
    //    //Mus där
    //    if (this.GetComponent<BoxCollider2D>().bounds.Contains(mouseHower.transform.position))
    //    {
    //        if (GameManager.Instance.ClickedShopPrefab != null && trainIsWithin == false)
    //        {
    //            if (HUDCounter.CurrencyCounter < GameManager.Instance.ClickedShopPrefab.PrefabCost)
    //            {
    //                ColorSlot(fullColor);
    //            }
    //            else if (HUDCounter.CurrencyCounter >= GameManager.Instance.ClickedShopPrefab.PrefabCost)
    //            {
    //                if (isEmpty && whatKindOfSlot == GameManager.Instance.ClickedShopPrefab.WhatObj)
    //                {
    //                    if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
    //                    {
    //                        slotExtendeSize = Camera.main.orthographicSize / 2;
    //                        this.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //                    }

    //                    if (currentObjectName == "Cart" && whatKindOfSlot == "Cart" && this.gameObject.tag == "EmptyCartSlot")
    //                    {
    //                        slotExtendeSize = Camera.main.orthographicSize / 4;
    //                        this.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //                    }

    //                    Debug.Log("snapa in spriten");
    //                    ColorSlot(emptyColor);
    //                    mouseHower.GetComponent<ImigeHower>().followMouse = false;
    //                    mouseHower.transform.position = this.transform.position;

    //                    if (!isEmpty)
    //                    {
    //                        ColorSlot(fullColor);
    //                    }

    //                    else if (Input.GetMouseButtonUp(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
    //                    {
    //                        if (arrowHandler != null)
    //                        {
    //                            if (currentObjectName == "Turret" && arrowHandler.GetComponent<INeedResults>().firstTurretPlaced == false)
    //                            {
    //                                arrowHandler.GetComponent<INeedResults>().firstTurretPlaced = true;
    //                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
    //                            }
    //                            if (currentObjectName == "Driller" && arrowHandler.GetComponent<INeedResults>().firstDrillerPlaced == false)
    //                            {
    //                                arrowHandler.GetComponent<INeedResults>().firstDrillerPlaced = true;
    //                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
    //                            }
    //                            if (currentObjectName == "Cart" && arrowHandler.GetComponent<INeedResults>().firstCartPlaced == false)
    //                            {
    //                                arrowHandler.GetComponent<INeedResults>().firstCartPlaced = true;
    //                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
    //                            }
    //                        }

    //                        if (this.transform.localScale.x > slotOriginalSize.x)
    //                        {
    //                            this.transform.localScale = slotOriginalSize;
    //                        }
    //                        PlaceTower();

    //                        if (this.gameObject.tag == "FirstExtension")
    //                        {
    //                            this.gameObject.GetComponent<ActivateExtension>().ActivatePointExtension();
    //                        }
    //                        if (this.gameObject.tag == "SecondExtension")
    //                        {
    //                            this.gameObject.GetComponent<ActivateEndExtension>().ActivateEndPointExtension();
    //                        }

    //                        this.gameObject.tag = "FullSlot";
    //                        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
    //                        gameObject.layer = 2;
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    //Mus ifrån
    //    if (Vector3.Distance(mouseHower.transform.position, Input.mousePosition) > 100)
    //    {
    //        if (GameManager.Instance.ClickedShopPrefab != null)
    //        {
    //            Debug.Log("gled isär för långt så hoppade");
    //            mouseHower.GetComponent<ImigeHower>().followMouse = true;
    //            if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
    //            {
    //                this.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //            }
    //            if (currentObjectName == "Cart" && whatKindOfSlot == "Cart" && this.gameObject.tag == "EmptyCartSlot")
    //            {
    //                this.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //            }

    //            if (HUDCounter.CurrencyCounter >= GameManager.Instance.ClickedShopPrefab.PrefabCost && isEmpty && whatKindOfSlot == GameManager.Instance.ClickedShopPrefab.WhatObj)
    //                mouseHower.GetComponent<ImigeHower>().followMouse = true;
    //        }

    //        if (whatKindOfSlot == "Cart" || whatKindOfSlot == "Railroad")
    //            ColorSlot(cartColor);
    //        else
    //            ColorSlot(normalColor);
    //    }
    //}

    //void OnMouseEnter()
    //{
    //    if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedShopPrefab != null)
    //    {
    //        if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
    //        {
    //            slotExtendeSize = Camera.main.orthographicSize / 2;
    //            this.transform.localScale += new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //        }
    //        if (currentObjectName == "Cart" && whatKindOfSlot == "Cart" && this.gameObject.tag == "EmptyCartSlot")
    //        {
    //            slotExtendeSize = Camera.main.orthographicSize / 4;
    //            this.transform.localScale += new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //        }
    //    }
    //}

    void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedShopPrefab != null && trainIsWithin == false)
        {
            if (HUDCounter.CurrencyCounter < GameManager.Instance.ClickedShopPrefab.PrefabCost)
            {
                ColorSlot(fullColor);
            }
            else if (HUDCounter.CurrencyCounter >= GameManager.Instance.ClickedShopPrefab.PrefabCost)
            {
                if (isEmpty && whatKindOfSlot == GameManager.Instance.ClickedShopPrefab.WhatObj && ReplaceTurret.ReplaceTurretMode == false)
                {
                    ColorSlot(emptyColor);
                    mouseHower.GetComponent<ImigeHower>().followMouse = false;
                    mouseHower.transform.position = this.transform.position;
                    if (!isEmpty)
                    {
                        ColorSlot(fullColor);
                    }

                    else if (Input.GetMouseButtonDown(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        if (arrowHandler != null)
                        {
                            if (currentObjectName == "Turret" && arrowHandler.GetComponent<INeedResults>().firstTurretPlaced == false)
                            {
                                arrowHandler.GetComponent<INeedResults>().firstTurretPlaced = true;
                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
                            }
                            if (currentObjectName == "Driller" && arrowHandler.GetComponent<INeedResults>().firstDrillerPlaced == false)
                            {
                                arrowHandler.GetComponent<INeedResults>().firstDrillerPlaced = true;
                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
                            }
                            if (currentObjectName == "Cart" && arrowHandler.GetComponent<INeedResults>().firstCartPlaced == false)
                            {
                                arrowHandler.GetComponent<INeedResults>().firstCartPlaced = true;
                                arrowHandler.GetComponent<INeedResults>().lookAt = null;
                            }
                        }

                        if (this.transform.localScale.x > slotOriginalSize.x)
                        {
                            this.transform.localScale = slotOriginalSize;
                        }
                        PlaceTower();

                        if (this.gameObject.tag == "FirstExtension")
                        {
                            this.gameObject.GetComponent<ActivateExtension>().ActivatePointExtension();
                        }
                        if (this.gameObject.tag == "SecondExtension")
                        {
                            this.gameObject.GetComponent<ActivateEndExtension>().ActivateEndPointExtension();
                        }

                        this.gameObject.tag = "FullSlot";
                        //this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                        //gameObject.layer = 2;
                    }
                }
                else if (!isEmpty && ReplaceTurret.ReplaceTurretMode == true)
                {
                    mouseHower.GetComponent<ImigeHower>().followMouse = false;

                    if (Input.GetMouseButtonDown(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
                    {

                        if (this.transform.localScale.x > slotOriginalSize.x)
                        {
                            this.transform.localScale = slotOriginalSize;
                        }
                        ReplaceTower();
                        
                    }
                }
            }
        }
    }

    //private void OnMouseExit()
    //{
    //    if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedShopPrefab != null)
    //    {
    //        if (currentObjectName == "Turret" && whatKindOfSlot == "Turret" && this.gameObject.tag == "EmptyTurretSlot")
    //        {
    //            this.transform.localScale -= new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //        }
    //        if (currentObjectName == "Cart" && whatKindOfSlot == "Cart" && this.gameObject.tag == "EmptyCartSlot")
    //        {
    //            this.transform.localScale -= new Vector3(slotExtendeSize, slotExtendeSize, 0);
    //        }

    //        if (HUDCounter.CurrencyCounter >= GameManager.Instance.ClickedShopPrefab.PrefabCost && isEmpty && whatKindOfSlot == GameManager.Instance.ClickedShopPrefab.WhatObj)
    //            mouseHower.GetComponent<ImigeHower>().followMouse = true;
    //    }

    //    if (whatKindOfSlot == "Cart" || whatKindOfSlot == "Railroad")
    //        ColorSlot(cartColor);
    //    else
    //        ColorSlot(normalColor);
    //}

    void PlaceTower()
    {
        if(HUDCounter.CurrencyCounter >= currentObjectCost && whatKindOfSlot == currentObjectName)
        {
            GameObject td = Instantiate(GameManager.Instance.ClickedShopPrefab.ShopPrefab, this.transform.position, Quaternion.identity) as GameObject;
            Turretsize = td.transform;
            td.transform.SetParent(this.transform);
            
            //td.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
            if (currentObjectName == "Cart" && whatKindOfSlot == "Cart")
             {

                 backsnapturret = td;
             }
            isEmpty = false;
            if(currentObjectName == "Turret")
            {
                td.gameObject.layer = 2;
                foreach (Transform trans in td.GetComponentsInChildren<Transform>(true))
                {
                    trans.gameObject.layer = 2;
                }
            }
            if (whatKindOfSlot == "Cart" || whatKindOfSlot == "Railroad")
                ColorSlot(cartColor);

            else
                ColorSlot(normalColor);

            GameManager.Instance.BuyPrefab();
            spriteRenderer.enabled = false;
            HUDCounter.CurrencyCounter -= currentObjectCost;
        }
    }
    void ReplaceTower()
    {
        if (HUDCounter.CurrencyCounter >= currentObjectCost && whatKindOfSlot == currentObjectName)
        {
            this.transform.GetChild(this.transform.GetChildCount() - 1).gameObject.SetActive(false);
            GameObject td = Instantiate(GameManager.Instance.ClickedShopPrefab.ShopPrefab, this.transform.position, Quaternion.identity) as GameObject;
            Turretsize = td.transform;
            td.transform.SetParent(this.transform);
            td.gameObject.layer = 2;
            foreach (Transform trans in td.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = 2;
            }
            //td.transform.localScale = new Vector3(slotExtendeSize, slotExtendeSize, 0);
            GameManager.Instance.BuyPrefab();
            //spriteRenderer.enabled = false;
            HUDCounter.CurrencyCounter -= currentObjectCost;
            ReplaceTurret.ReplaceTurretMode = false;
        }
    }


    public void ColorSlot(Color newColor)
    {
        spriteRenderer.color = newColor;
    }

    public string WhatKindOfSlot()
    {
        return whatKindOfSlot;
    }


}