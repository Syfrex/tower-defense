﻿using UnityEngine;
using System.Collections;

public class SplineCart : MonoBehaviour
{
            //Variables for FunctionCode
        //Publics
    public BezierSpline spline;
    public SplineWalkerMode mode;
    public bool lookForward;
    public float duration;
        //Privates
    private float progress;
    private bool goingForward = true;
    SplineWalker splineParent;

            //Start of FunctionCode
        //Privates
    private void Start()
    {
        spline = splineParent.spline;
        lookForward = true;
        mode = SplineWalkerMode.Loop;

        this.transform.parent = null;
    }

    private void Update()
    {
        if(goingForward)
        {
            progress += Time.deltaTime / duration;
            if (progress > 1f)
            {
                if(mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if(mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else if(mode == SplineWalkerMode.Reverse)
                {
                    progress = 2f;
                }
                else
                {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else
        {
            progress -= Time.deltaTime / duration;
            if(progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            //transform.LookAt(position + spline.GetDirection(progress)); //for 3D Obj

            Vector3 dirr = position - spline.GetDirection(progress);
            float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
            this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

        //Publics

}
