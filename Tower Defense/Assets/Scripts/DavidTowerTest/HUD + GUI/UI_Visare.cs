﻿using UnityEngine;
using System.Collections;

public class UI_Visare : MonoBehaviour
{
    public static Transform Target;
    public Transform StartPedel;

    void Start()
    {
        Target = StartPedel;
    }

    void LateUpdate()
    {
        Vector3 dirr = Target.position - this.transform.position;
        float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
        this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}