﻿using UnityEngine;
using System.Collections;

public class ImigeHower : Singleton<ImigeHower>
{
    private SpriteRenderer spriteRender;
    private GameObject arrow;
    public bool followMouse;

    [SerializeField]
    GameObject canvas;

    [SerializeField]
    private GameObject[] RailExtensions;
    [SerializeField]
    private GameObject[] SecondRailExtensions;

    private bool railExtend = false;
    public static bool clickingoutsidetrain = false;
    private bool waitforbullettime = false;
    private float distancetoTurret;


    void Start ()
    {
        followMouse = false;
        arrow = GameObject.Find("ArrowHandler");
        this.spriteRender = GetComponent<SpriteRenderer>();
        
    }

    void Update()
    {
        if (followMouse == true)
            FollowMouse();


        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            if (followMouse == true)
                BenethFinger();
                //FollowMouse();
        }
        if (Input.touchCount == 1 && GameManager.Instance.ClickedShopPrefab != null && TouchCamera.isShopping == true && waitforbullettime == true || Input.GetMouseButtonDown(0) && GameManager.Instance.ClickedShopPrefab != null && TouchCamera.isShopping == true && waitforbullettime == true)
        {

            GameObject[] turretslots = GameObject.FindGameObjectsWithTag("EmptyTurretSlot");
            float ShortestDistance = Mathf.Infinity;
            GameObject NearestTurret = null;
            foreach (GameObject turretslot in turretslots)
            {
                distancetoTurret = Vector3.Distance(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0), new Vector3(turretslot.transform.position.x, turretslot.transform.position.y, 0));
                if (distancetoTurret < ShortestDistance)
                {
                    ShortestDistance = distancetoTurret;
                    NearestTurret = turretslot;
                }
            }

            if(distancetoTurret > 3)
            {

                waitforbullettime = false;
                Deactivate();
            }

        }
        //Debug.Log(waitforbullettime);

        //Debug.Log(Vector3.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.ScreenToWorldPoint(GameObject.Find("Canvas").transform.FindChild("ReplaceTurret").transform.position)));
        //if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
        //{
        //    Deactivate();
        //}
        if(clickingoutsidetrain == true)
        {

            Deactivate();
            clickingoutsidetrain = false;
        }
    }

    private void FollowMouse()
    {
        if(spriteRender.enabled)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x /*- 0.7f*/, transform.position.y + 1f, 0);
            if (Time.timeScale <= 1 && Time.timeScale > 0.15)
                Time.timeScale -= 0.01f;
            if (Time.timeScale <= 0.15 && Vector3.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.ScreenToWorldPoint(/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").transform.position)) > 2)
            {
                waitforbullettime = true;
            }
            else if(Time.timeScale <= 0.15 && Vector3.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.ScreenToWorldPoint(/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").transform.position)) < 2)
            {
                waitforbullettime = false;
            }
        }
    }
    private void BenethFinger()
    {
        if (spriteRender.enabled)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            //transform.position = new Vector3(transform.position.x /*- 0.7f*/, transform.position.y + 1f, 0);
            if (Time.timeScale <= 1 && Time.timeScale > 0.15)
                Time.timeScale -= 0.01f;
            if (Time.timeScale <= 0.15 && Vector3.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.ScreenToWorldPoint(/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").transform.position)) > 2)
            {
                waitforbullettime = true;
            }
            else if (Time.timeScale <= 0.15 && Vector3.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.ScreenToWorldPoint(/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").transform.position)) < 2)
            {
                waitforbullettime = false;
            }
        }
    }

    public void Activate(Sprite sprite)
    {
        TouchCamera.isShopping = true;
        this.spriteRender.sprite = sprite;
        spriteRender.enabled = true;
        if (/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").gameObject.activeSelf == false)
        {
            /*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").gameObject.SetActive(true);
        }
        if (/*GameObject.Find("Canvas")*/canvas.transform.Find("PlaceAtEmptySlot").gameObject.activeSelf == false)
        {
            /*GameObject.Find("Canvas")*/canvas.transform.Find("PlaceAtEmptySlot").gameObject.SetActive(true);
        }
        Time.timeScale = 0.3f;
        followMouse = true;
        if(arrow != null)
            arrow.GetComponent<INeedResults>().turnOn = true;

        if(GameManager.Instance.ClickedShopPrefab.WhatObj == "Railroad")
        {
            railExtend = true;

            foreach (GameObject ex in RailExtensions)
            {
                if (ex.tag == "FirstExtension")
                {
                    ex.SetActive(true);
                }
            }
            foreach (GameObject ex2 in SecondRailExtensions)
            {
                if (ex2.tag == "SecondExtension")
                {
                    ex2.GetComponent<FailSafeActivateExtension>().CheckVisibility(true);
                }
            }
        }
    }

    public void Deactivate()
    {
        if(Time.timeScale < 1.0f)
        {
            Time.timeScale = 1.0f;
            waitforbullettime = false;
        }
        if (/*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").gameObject.activeSelf == true)
        {
            /*GameObject.Find("Canvas")*/canvas.transform.Find("ReplaceTurret").gameObject.SetActive(false);
        }
        if (/*GameObject.Find("Canvas")*/canvas.transform.Find("PlaceAtEmptySlot").gameObject.activeSelf == true)
        {
            /*GameObject.Find("Canvas")*/canvas.transform.Find("PlaceAtEmptySlot").gameObject.SetActive(false);
        }
        TouchCamera.isShopping = false;
        GameManager.Instance.ClickedShopPrefab = null;
        spriteRender.enabled = false;
        followMouse = false;
        if (arrow != null)
        {
            arrow.GetComponent<INeedResults>().turnOn = false;
        }

        if(railExtend == true)
        {
            foreach (GameObject ex in RailExtensions)
            {
                if (ex.tag == "FirstExtension")
                {
                    ex.SetActive(false);
                }
            }
            foreach (GameObject ex2 in SecondRailExtensions)
            {
                if (ex2.tag == "SecondExtension")
                {
                    ex2.GetComponent<FailSafeActivateExtension>().CheckVisibility(false);
                }
            }

            railExtend = false;
        }
    }
}