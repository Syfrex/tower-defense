﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class HideAndShowShopmenu : MonoBehaviour
{
    public GameObject shopMenu;
    public GameObject shopPopUpTurretsMenu;
    public GameObject shopPopUpResourcesMenu;
    public GameObject shopPopUpCartMenu;

    public void OpenCloseShop()
    {
        shopPopUpTurretsMenu.SetActive(false);
        shopPopUpResourcesMenu.SetActive(false);
        shopPopUpCartMenu.SetActive(false);
        shopMenu.GetComponent<Animator>().SetBool("Open", !shopMenu.GetComponent<Animator>().GetBool("Open"));
    }
}
