﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TowerMenuButton : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private GameObject shopPrefab;

    [SerializeField]
    private GameObject shopPrefabText;

    [SerializeField]
    private Sprite prefabSprite;

    [SerializeField]
    private string whatObject;

    [SerializeField]
    private int prefabCost;

    void Start()
    {
        this.GetComponentInChildren<Text>().text = this.GetComponent<FUCKMESCRIPT>().costOfPrefab.ToString();
        prefabCost = this.GetComponent<FUCKMESCRIPT>().costOfPrefab;
    }

    void Update()
    {
        if(HUDCounter.CurrencyCounter < prefabCost)
        {
            this.GetComponent<Image>().color = Color.red;
            if(shopPrefabText != null)
                shopPrefabText.SetActive(false);
        }
        if (HUDCounter.CurrencyCounter >= prefabCost)
        {
            this.GetComponent<Image>( ).color = Color.white;
            if (shopPrefabText != null)
                shopPrefabText.SetActive(true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(this.GetComponent<Image>().color == Color.white)
        {
            GameManager.Instance.PickTower(this.GetComponent<TowerMenuButton>());
            this.transform.parent.gameObject.SetActive(false);
        }
    }

    public GameObject ShopPrefab
    {
        get
        {
            return shopPrefab;
        }
    }

    public Sprite Sprite
    {
        get
        {
            return prefabSprite;
        }
    }

    public string WhatObj
    {
        get
        {
            return SlotScript.currentObjectName = whatObject;
        }
    }

    public string WhatKindOfShopItem()
    {
        return whatObject;
    }

    public int PrefabCost
    {
        get
        {
            return SlotScript.currentObjectCost = prefabCost;
        }
    }
}