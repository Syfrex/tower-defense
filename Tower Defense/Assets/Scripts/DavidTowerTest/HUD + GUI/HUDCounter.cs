﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDCounter : MonoBehaviour
{
    public bool Currency = false;
    public bool Wave = false;
    public int startCurrency;
    public int startWave;
    public static int WaveCounter;
    public static int CurrencyCounter;

    void Start()
    {
        if(Wave == true)
            WaveCounter = startWave;
        if(Currency == true)
            CurrencyCounter = startCurrency;
    }

    void LateUpdate ()
    {
        if(Currency == true)
        {
            GetComponent<Text>().text = CurrencyCounter.ToString();
        }

        if(Wave == true)
        {
            GetComponent<Text>().text = WaveCounter.ToString();
        }

    }
}
