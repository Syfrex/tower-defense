﻿using UnityEngine;
using System.Collections;

public class TrainMenuPedel : MonoBehaviour
{
    [SerializeField]
    private float determineSpeed;

    public Transform acce;
    public Transform stop;

    public float DetermineSpeed
    {
        get
        {
            //return TrainScript.currentTrainSpeed = determineSpeed;
            //return GetDirectionFromNode.currentSpeed = determineSpeed;
            return Train_GetDirectionFromNodes.currentSpeed = determineSpeed;
        }
    }

    public void SetTrue()
    {
        TrainScript.alterTrainSpeed = true;
        GetDirectionFromNode.alteredSpeed = true;
        Train_GetDirectionFromNodes.alteredSpeed = true;
    }

    public void SetTarget()
    {
        UI_Visare.Target = this.transform;
    }
}