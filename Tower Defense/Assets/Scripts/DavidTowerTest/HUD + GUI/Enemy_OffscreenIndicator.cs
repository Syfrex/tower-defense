﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Enemy_OffscreenIndicator : MonoBehaviour
{
    //public Transform center; //obj to circle
    //public float degreesPerSecond = -65.0f; //turning angle
    //public float circleSpeed;
    //private Vector3 distance;

    //// Use this for initialization
    //void Start()
    //{
    //    distance = transform.position - center.position; //distance from center
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    //Quaterion is a rotation. It does nothing by itself and you cannot assign a Quaternion to a vector3.
    //    //But if you multiply a Vector3 by a Quaternion, then you the result is a Vector3 rotated by the Queaternion
    //    distance = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime * circleSpeed, Vector3.forward) * distance;
    //    transform.position = center.position + distance;
    //}
    public GameObject waveSpawnpointsParent;
    private List<GameObject> waves;
    private List<GameObject> spawnpoint;
    private Transform target;

    void Start()
    {
        foreach (Transform wave in waveSpawnpointsParent.GetComponentInChildren<Transform>())
        {
            if (wave.tag == "Wave")
            {
                waves.Add(wave.gameObject);
            }
        }
    }
    
    void Update()
    {

    }

    void CheckWave(int w)
    {
        foreach (Transform sp in waves[w].GetComponentInChildren<Transform>())
        {
            if (sp.tag == "EnemySpawn")
            {
                spawnpoint.Add(sp.gameObject);
            }
        }
    }
}           