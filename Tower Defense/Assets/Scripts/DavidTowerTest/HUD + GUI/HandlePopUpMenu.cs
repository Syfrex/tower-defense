﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class HandlePopUpMenu : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    public GameObject ifThisOneNotActive;
    public GameObject andNeitherThisOne;
    public GameObject thenActiveThis;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if(!ifThisOneNotActive.activeInHierarchy && !andNeitherThisOne.activeInHierarchy)
        //{
        //    thenActiveThis.SetActive(!thenActiveThis.activeInHierarchy);

        //}

        //if (ifThisOneNotActive.activeInHierarchy || andNeitherThisOne.activeInHierarchy)
        //{
        //    ifThisOneNotActive.SetActive(false);
        //    andNeitherThisOne.SetActive(false);
        //    thenActiveThis.SetActive(true);
        //}
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!ifThisOneNotActive.activeInHierarchy && !andNeitherThisOne.activeInHierarchy)
        {
            thenActiveThis.SetActive(!thenActiveThis.activeInHierarchy);

        }

        if (ifThisOneNotActive.activeInHierarchy || andNeitherThisOne.activeInHierarchy)
        {
            ifThisOneNotActive.SetActive(false);
            andNeitherThisOne.SetActive(false);
            thenActiveThis.SetActive(true);
        }
    }
}
