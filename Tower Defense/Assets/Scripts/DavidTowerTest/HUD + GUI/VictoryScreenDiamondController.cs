﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VictoryScreenDiamondController : MonoBehaviour
{
    private float baseHealth;

    public Sprite rating1;
    public Sprite rating2;
    public Sprite rating3;

    void Start ()
    {
        baseHealth = MineScript.Health;

        if (baseHealth >= 8)
        {
            this.GetComponent<Image>().sprite = rating3;
        }
        if (baseHealth <= 7 && baseHealth >= 5)
        {
            this.GetComponent<Image>().sprite = rating2;
        }
        if(baseHealth <= 4) 
        {
            this.GetComponent<Image>().sprite = rating1;
        }
    }
}
