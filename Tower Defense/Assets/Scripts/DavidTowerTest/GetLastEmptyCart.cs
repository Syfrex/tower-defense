﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLastEmptyCart : MonoBehaviour
{
    private GameObject emptyCartSlot;
    
    public void GetEmptyCartSlotAndPlaceCartThere()
    {
        Debug.Log("Clicked shop Item: " + GameManager.Instance.ClickedShopPrefab.ShopPrefab);

        emptyCartSlot = GameObject.FindGameObjectWithTag("EmptyCartSlot");

        Debug.Log("An empty slot to place it upon: " + emptyCartSlot);

        GameObject td = Instantiate(GameManager.Instance.ClickedShopPrefab.ShopPrefab, emptyCartSlot.transform.position, Quaternion.identity) as GameObject;
        td.transform.SetParent(emptyCartSlot.transform);

        GameManager.Instance.BuyPrefab();
        HUDCounter.CurrencyCounter -= 40;
        emptyCartSlot.GetComponent<SpriteRenderer>().enabled = false;
        emptyCartSlot.tag = "FullSlot";
    }
}
