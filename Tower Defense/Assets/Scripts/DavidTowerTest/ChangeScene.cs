﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class ChangeScene : MonoBehaviour
{
    [SerializeField]
    private GameObject menuPanelInCanvas;
    public string GoToScene;

    public void OpenClosePanel()
    {
        if(menuPanelInCanvas != null)
            menuPanelInCanvas.SetActive(!menuPanelInCanvas.activeInHierarchy);
    }

    public void ChangeSceneTo()
    {
        SceneManager.LoadScene(GoToScene, LoadSceneMode.Single);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LaunchWebpage()
    {
        Application.OpenURL("https://goo.gl/TuA8va");
    }
}
