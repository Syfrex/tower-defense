﻿using UnityEngine;
using System.Collections;

public class INeedResults : MonoBehaviour
{
    public GameObject turretSlot;
    public GameObject drillSlot;
    public GameObject cartSlot;

    public bool firstTurretPlaced = false;
    public bool firstDrillerPlaced = false;
    public bool firstCartPlaced = false;

    public bool turnOn = false;

    private SpriteRenderer spriteRender;

    public GameObject lookAt = null;

    void Start ()
    {
        this.spriteRender = GetComponent<SpriteRenderer>();
        spriteRender.enabled = turnOn;
    }

    void Update ()
    {
        spriteRender.enabled = turnOn;
        if(turnOn == true)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, transform.position.y + 1, 0);

            Vector3 dirr = lookAt.transform.position - this.transform.position;
            float angle = Mathf.Atan2(dirr.y, dirr.x) * Mathf.Rad2Deg;
            this.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        }

        if (firstDrillerPlaced == true && firstTurretPlaced == true && firstCartPlaced == true)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void EnableTurretArrow ()
    {
        if(firstTurretPlaced == false)
        {
            lookAt = turretSlot;
        }
    }

    public void EnableDrillArrow ()
    {
        if (firstDrillerPlaced == false)
        {
            lookAt = drillSlot;
        }
    }

    public void EnableCartArrow ()
    {
        if(firstCartPlaced == false)
        {
            lookAt = cartSlot;
        }
    }
}