﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ReplaceTurret : MonoBehaviour {

    public static bool ReplaceTurretMode = false;
    public Color StandardColor;
    public Color SelectedColor;

    void Start()
    {
        InvokeRepeating("CheckColor", 0.0f, 0.2f);
    }
    public void Replacemode()
    {
        if (ReplaceTurretMode == false)
        {
            ReplaceTurretMode = true;
            this.transform.GetComponent<Image>().color = SelectedColor;
        }
        else
        {
            ReplaceTurretMode = false;
            this.transform.GetComponent<Image>().color = StandardColor;
        }
    }
    void CheckColor()
    {
        if(ReplaceTurretMode == false && this.transform.GetComponent<Image>().color == SelectedColor)
            {
                this.transform.GetComponent<Image>().color = StandardColor;
            }
        
    }
}
