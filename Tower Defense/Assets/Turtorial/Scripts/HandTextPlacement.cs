﻿using UnityEngine;
using System.Collections;

public class HandTextPlacement : MonoBehaviour
{
	void Update ()
    {
        if (this.transform.parent.position.x > Screen.width * 0.6f)
            Vector2.Lerp(this.GetComponent<RectTransform>().anchoredPosition, this.GetComponent<RectTransform>().anchoredPosition = new Vector2(-119, -84), 1f * Time.deltaTime);

        if (this.transform.parent.position.x < Screen.width * 0.4f)
            Vector2.Lerp(this.GetComponent<RectTransform>().anchoredPosition, this.GetComponent<RectTransform>().anchoredPosition = new Vector2(113, -84), 1f * Time.deltaTime);
    }
}
