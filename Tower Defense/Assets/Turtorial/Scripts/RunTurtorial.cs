﻿using UnityEngine;
using System.Collections;

public class RunTurtorial : MonoBehaviour
{
    public void RunTurtotial()
    {
        TurtorialManager.turtorialRunOnce = false;
    }

    public void DontRunTurtotial()
    {
        TurtorialManager.turtorialRunOnce = true;
    }
}
