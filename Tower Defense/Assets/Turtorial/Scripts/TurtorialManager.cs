﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurtorialManager : MonoBehaviour
{
    // Privates
    private Vector2 targetDest;
    private Vector2 turtDiamondPos;
    private Vector2 trainPos;
    private bool clear = false;
    [HideInInspector]
    public static bool turtorialRunOnce;
    private float hudCountdown = 2.5f;
    WaveHandler waveHandler = new WaveHandler();
    private bool failsafeWavehandler = false;

    // Publics
    public static bool pressedTurtDiamond;
    //[Header("Main Camera Camerascript")]
    //public GameObject cameraScript;
    //public bool mobile;
    //public bool pc;
    [Header("World GameObjects")]
    public GameObject turtDiamond;
    public GameObject trainPrefab;
    public GameObject centralMineSpawnPoint;
    //public GameObject waveHandler;
    public GameObject enemyWaveSpawnpoints;
    public GameObject firstTrainSlot;
    public GameObject secondTrainSlot;
    public GameObject thirdTrainSlot;

    [Header("Screen UIObjects")]
    public GameObject preTextPrefab;
    public Text handTextPrefab;
    public GameObject marioHandPrefab;
    public GameObject currencyHUD;
    public GameObject firstAnim;
    public GameObject secondAnim;
    public GameObject thirdAnim;
    public GameObject shopPlankAnim;
    public GameObject hudPlankAnim;
    public GameObject pedelPlankAnim;
    public GameObject hideResourcePlank;
    public GameObject hideCartsPlank;

    [Header("ScreenPosition")]
    public Vector2 firstPos;
    public Vector2 secondPos;
    public Vector2 thirdPos;
    public Vector2 fourthPos;
    public Vector2 fifthPos;
    public Vector2 sixthPos;

    void Awake()
    {
        if (turtorialRunOnce == true)
        {
            centralMineSpawnPoint.SetActive(true);
            //waveHandler.SetActive(true);
            enemyWaveSpawnpoints.SetActive(true);
            //if (mobile == true)
            //    cameraScript.GetComponent<TouchCamera>().enabled = true;
            //if (pc == true)
            //    cameraScript.GetComponent<CameraScript>().enabled = true;
            GameObject.Destroy(preTextPrefab);
            GameObject.Destroy(turtDiamond);
            GameObject.Destroy(marioHandPrefab);
            GameObject.Destroy(firstAnim);
            GameObject.Destroy(secondAnim);
            GameObject.Destroy(thirdAnim);
            GameObject.Destroy(shopPlankAnim);
            GameObject.Destroy(hudPlankAnim);
            GameObject.Destroy(pedelPlankAnim);
            GameObject.Destroy(hideResourcePlank);
            GameObject.Destroy(hideCartsPlank);
            GameObject.Destroy(this);
        }
    }

    void Start ()
    {
        targetDest = new Vector2(292, -150);
        pressedTurtDiamond = false;
        WaveHandler.cooldown = 5000;
    }

    void LateUpdate ()
    {
        if(pressedTurtDiamond == true && hudPlankAnim != null)
        {
            hudPlankAnim.GetComponent<Animator>().SetBool("Fall", true);
            GameObject.Destroy(hudPlankAnim, hudPlankAnim.GetComponent<Animation>().clip.length);
        }

        if (turtDiamond == null && clear == false)
        {
            hudCountdown -= Time.deltaTime;
            if(hudCountdown < 0)
            {
                currencyHUD.GetComponent<Animator>().SetBool("FirstCollect", false);
                SecondTurtorialEvent();
                clear = true;
            }
        }

        if (firstTrainSlot.tag == "FullSlot" || secondTrainSlot.tag == "FullSlot" || thirdTrainSlot.tag == "FullSlot")
        {
            marioHandPrefab.SetActive(false);
            //waveHandler.SetActive(true);
            enemyWaveSpawnpoints.SetActive(true);
        }

        if (HUDCounter.WaveCounter == 2)
            SixthTurtorialEvent();

        marioHandPrefab.GetComponent<RectTransform>().anchoredPosition = 
            Vector2.Lerp(marioHandPrefab.GetComponent<RectTransform>().anchoredPosition, targetDest, 4 * Time.deltaTime);
    }

    public void FirstTurtorialEvent()
    {
        turtDiamond.SetActive(true);
        preTextPrefab.SetActive(false);
        targetDest = firstPos;
        handTextPrefab.text = "Tap to collect Diamond";
        //turtDiamondPos = Camera.main.WorldToScreenPoint(turtDiamond.transform.position);
        //targetDest = turtDiamondPos;
        //targetDest.x -= 20;
    }

    public void SecondTurtorialEvent()
    {
        firstAnim.SetActive(true);
        targetDest = secondPos;
        shopPlankAnim.GetComponent<Animator>().SetBool("Fall", true);
        GameObject.Destroy(shopPlankAnim, shopPlankAnim.GetComponent<Animation>().clip.length);
        handTextPrefab.text = "Spend Diamonds at the Shop";
    }

    public void ThirdTurtorialEvent()
    {
        GameObject.Destroy(firstAnim);
        if(secondAnim != null)
            secondAnim.SetActive(true);
        targetDest = thirdPos;
        handTextPrefab.text = "Tap to Open Turretlist";
    }

    public void FourthTurtorialEvent()
    {
        GameObject.Destroy(secondAnim);
        if (thirdAnim != null)
            thirdAnim.SetActive(true);
        targetDest = fourthPos;
        handTextPrefab.text = "Tap to Select Turret";
    }

    public void FifthTurtorialEvent()
    {
        centralMineSpawnPoint.SetActive(true);
        GameObject.Destroy(thirdAnim);
        trainPos = fifthPos;
        targetDest = trainPos;
        handTextPrefab.text = "Tap to Place Turret";
        if(failsafeWavehandler == false)
        {
            WaveHandler.cooldown = 8;
            failsafeWavehandler = true;
        }
    }

    public void SixthTurtorialEvent()
    {
        if(!marioHandPrefab.activeInHierarchy)
            marioHandPrefab.SetActive(true);
        targetDest = sixthPos;
        if(pedelPlankAnim != null)
        {
            pedelPlankAnim.GetComponent<Animator>().SetBool("Fall", true);
            GameObject.Destroy(pedelPlankAnim, pedelPlankAnim.GetComponent<Animation>().clip.length);
        }
        handTextPrefab.text = "Tap to Move the Train";
    }

    public void AllCleared()
    {
        turtorialRunOnce = true;
        //if(mobile == true)
        //    cameraScript.GetComponent<TouchCamera>().enabled = true;
        //if (pc == true)
        //    cameraScript.GetComponent<CameraScript>().enabled = true;
        GameObject.Destroy(marioHandPrefab);
        GameObject.Destroy(hideResourcePlank);
        GameObject.Destroy(hideCartsPlank);
        GameObject.Destroy(this.gameObject);
    }
}