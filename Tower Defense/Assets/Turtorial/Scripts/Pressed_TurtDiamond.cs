﻿using UnityEngine;
using System.Collections;

public class Pressed_TurtDiamond : MonoBehaviour
{
    public GameObject hudCounter;
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TurtorialManager.pressedTurtDiamond = true;
            hudCounter.GetComponent<Animator>().SetBool("FirstCollect", true);
        }
    }
}
