﻿using UnityEngine;
using System.Collections;

public class ShieldScript : MonoBehaviour 
{
    public GameObject BulletImpactEffect;
    void Start()
    {
     
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
       if(coll.gameObject.tag == "Bullet")
       {

           GameObject effectIns = (GameObject)Instantiate(BulletImpactEffect, new Vector3(transform.position.x, transform.position.y, -3), transform.rotation);
          // effectIns.transform.parent = this.transform.parent;
           transform.parent.GetComponent<DrillbossHealth>().ShieldHealth -= coll.gameObject.transform.GetComponent<Bullet>().parentobject.GetComponent<Turret>().Damage;
           Destroy(effectIns, 1f);
           Destroy(coll.gameObject);

       }
    }
    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Bullet")
        {

            GameObject effectIns = (GameObject)Instantiate(BulletImpactEffect, new Vector3(transform.position.x, transform.position.y, -3), transform.rotation);
            transform.parent.GetComponent<DrillbossHealth>().ShieldHealth -= coll.gameObject.transform.GetComponent<Bullet>().parentobject.GetComponent<Turret>().Damage;
           // effectIns.transform.parent = this.transform.parent;

            Destroy(effectIns, 1f);
            Destroy(coll.gameObject);

        }
    }

	void Update () 
    {

        if (transform.parent.GetComponent<DrillbossHealth>().ShieldHealth <= 0)
        {
            Destroy(this.gameObject);
        }
	
	}
}
